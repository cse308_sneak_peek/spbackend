package com.example.Servlets;

import com.example.saveObj.ComicObject;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by 배요한 (Joan Bae) on 2016-05-15.
 */
public class AuthorPageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String author = req.getParameter("author");
        List<ComicObject> comics = ObjectifyService.ofy()
                .load()
                .type(ComicObject.class)
                .filter("authorName", author)
                .list();

        for(ComicObject a:comics){
            System.out.println("the name is "+a.getComicName());
        }
        req.setAttribute("authorName",author);
        req.setAttribute("authorcomics",comics);
        req.getRequestDispatcher("AuthorPage.jsp").forward(req, resp);
    }
}
