package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicCommentObject;
import com.example.saveObj.ComicObject;
import com.example.saveObj.SubscriptionObject;
import com.example.saveObj.UserDatastore;
import com.example.saveObj.WorkshopObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class ChapterPageServlet extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ComicObject comic = (ComicObject) req.getSession().getAttribute("currentComic");
		ChapterObject chapter = ObjectifyService
				.ofy()
				.load()
				.type(ChapterObject.class)
				.ancestor(comic)
				.filter("chapterNum",
						Integer.parseInt(req.getParameter("chapterNum")))
				.first().now();

		req.getSession().setAttribute("currentChapter", chapter);
		req.getRequestDispatcher("/workzoom.jsp").forward(req, resp);
		//req.getRequestDispatcher("/uploadComic.jsp").forward(req,resp);
    }

	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ChapterObject chapter = (ChapterObject) req.getSession().getAttribute("currentChapter");
		System.out.println("==========================="+chapter.getChapterName()+"========================");
		//String newPage = req.getParameter("newPage");
		//String newPageLoad = req.getParameter("newPageLoad");
		//System.out.println("-------------------- JSON ---------------\n " + newPageLoad + "\n--------------------------------------");
		//System.out.println(newPage);
		if(chapter!=null){
			
			if(chapter.getChapterPagesLoad()==null){
				List<String> newLoadList = new ArrayList<String>();
				newLoadList.add(req.getParameter("newPageLoad"));
				chapter.setChapterPagesLoad(newLoadList);
			}
			else{
				chapter.getChapterPagesLoad().add(req.getParameter("newPageLoad"));
			}
			if(chapter.getChapterPages()==null){
				List<String> newList = new ArrayList<String>();
				newList.add(req.getParameter("newPage"));
				chapter.setChapterPages(newList);
			}
			else{
				chapter.getChapterPages().add(req.getParameter("newPage"));
			}
			
			//System.out.println("-------------------- Base64 ---------------\n " + newPage + "\n--------------------------------------");
			//System.out.println("-------------------- JSON ---------------\n " + newPageLoad + "\n--------------------------------------");
			//chapter.getChapterPages().add(req.getParameter("newPage"));
			//chapter.addChapterPage(req.getParameter("newPage"));
			//chapter.addChapterPageLoad(req.getParameter("newPageLoad"));
			chapter.setNumOfPages(chapter.getNumOfPages()+1);
			ObjectifyService.ofy().save().entity(chapter).now();
		}
		req.getSession().setAttribute("PASSING", "A");
		//req.getRequestDispatcher("/workshop").forward(req, resp);
		resp.sendRedirect("/workshop");
		//req.getRequestDispatcher("workzoom.jsp").forward(req, resp);
		

    }

}
