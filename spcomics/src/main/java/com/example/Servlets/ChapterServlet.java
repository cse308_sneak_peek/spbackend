package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicObject;
import com.example.saveObj.HistoryObject;
import com.example.saveObj.UserDatastore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Key;

import java.util.List;
import java.util.Date;
import java.io.IOException;

public class ChapterServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserDatastore user = (UserDatastore)req.getSession().getAttribute("user_info");

		ComicObject comic = ObjectifyService.ofy()
							.load()
							.type(ComicObject.class)
							.filter("comicId", Long.parseLong(req.getParameter("comicId")))
							.first()
							.now();

		ChapterObject chapter = ObjectifyService.ofy()
								.load()
								.type(ChapterObject.class)
								.ancestor(comic)
								.filter("chapterNum", Integer.parseInt(req.getParameter("chapterNum")))
								.first()
								.now();

		if(user != null) {
			keepHistory(user.getUserId(), comic.getComicId().longValue(), comic.getComicName(), chapter.getChapterNum(), chapter.getChapterName());
		}

		//req.getRequestDispatcher("/comicpage.jsp").forward(req, resp);
	}

	public void keepHistory(String userId, Long comicId, String comicName, int chapterNum, String chapterName) {
		/* keep the reading history */
		System.out.println("parameters in keepHistory: "+userId+", "+comicId+", "+chapterNum);
		List<HistoryObject> userHistories = ObjectifyService.ofy()
											.load()
											.type(HistoryObject.class)
											.filter("userId", userId)
											.list();

		HistoryObject history=null;
		for(HistoryObject userHistory : userHistories) {
			if((comicId == userHistory.getComicId().longValue()) && (chapterNum == userHistory.getChapterNum())) {
				history = userHistory;
				break;
			}
		}

		if(history == null) {
			history = new HistoryObject(userId, comicId, comicName, chapterNum, chapterName);
		} else {
			history.setViewedDate();
		}
		ObjectifyService.ofy().save().entity(history).now();

		return;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ComicObject comic = ObjectifyService.ofy()
				.load()
				.type(ComicObject.class)
				.filter("comicId", Long.parseLong(req.getParameter("comicId")))
				.first()
				.now();

		List<ChapterObject> chapterList = ObjectifyService.ofy()
				.load()
				.type(ChapterObject.class)
				.ancestor(comic)
				.order("-chapterNum")
				.list();

		// Check there is chapter with same name
		String chapterName = req.getParameter("chapterName");
		boolean sameChapFlag = false;
		for(ChapterObject chapterObject : chapterList) {
			if(chapterObject.getChapterName().equals(chapterName)) {
				System.out.println("   GIVEN CHAPTER NAME IS ALREADY EXISTED!!   ");
				sameChapFlag = true;
			}
		}

		if(!sameChapFlag) {
			// Find new chapter number automatically
			int chapterNum;
			if(chapterList.size() > 0) {
				chapterNum = chapterList.get(0).getChapterNum() + 1;
			} else {
				chapterNum = 1;
			}

			ChapterObject chapter = new ChapterObject(chapterNum, chapterName, comic.getComicId());
			ObjectifyService.ofy().save().entity(chapter).now();
			
			/*
			Key<ComicObject> key = Key.create(chapter).getParent();
			System.out.println(" new chapter added under:  " + key);
<<<<<<< HEAD
=======
			ComicObject parentComic = ObjectifyService.ofy().load().key(key).now();
			System.out.println(" new chapter added under comic:  " + parentComic.getComicName());
			*/
		}

		req.setAttribute("comicId", req.getParameter("comicId"));
		req.getRequestDispatcher("/uploadComic.jsp").forward(req, resp);

	}

}