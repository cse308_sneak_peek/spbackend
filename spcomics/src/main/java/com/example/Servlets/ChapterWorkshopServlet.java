package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicCommentObject;
import com.example.saveObj.ComicObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Key;

import java.util.List;
import java.util.Date;
import java.io.IOException;

public class ChapterWorkshopServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("/workzoom.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		ComicObject comic = (ComicObject) req.getSession().getAttribute("currentComic");
		Long comicId = Long.parseLong(req.getParameter("comicId"));
		int chapterNum = Integer.parseInt(req.getParameter("chapNum"));
		String chapterName=req.getParameter("chapterName");
		String inputPage=req.getParameter("fileInput");

		comic.setNumOfChapters(chapterNum);
		//comic.setLatestChaptername(chapterName);
		ChapterObject chapter = new ChapterObject(chapterNum,chapterName,comicId);
		chapter.addChapterPage(inputPage);
		chapter.setSeriesName(comic.getComicName());
		ObjectifyService.ofy().save().entities(chapter).now();
		ObjectifyService.ofy().save().entities(comic).now();
		/**
		 * What's going to happen is that when the chapter number is clicked, the page will refresh and load the workzoom.jsp (AKA Canvas Page)
		 * On the right side will exist the chapter pages in an ordered list which will be retrieved with JSTL in workzoom.jsp
		 * When one of the pages is clicked, the canvas will update accordingly and we MUST keep track of which list item was clicked so we know which
		 * item in the list to alter when the "Save Chapter" button is clicked
		 */
		//chapter.getChapterPages().add(req.getParameter("newPage"));
		req.getRequestDispatcher("/UploadChapter.jsp").forward(req, resp);

	}

}
