package com.example.Servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicObject;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.ObjectifyService;

public class ComicCoverServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//ChapterObject chapter = (ChapterObject) req.getSession().getAttribute("currentChapter");
		ComicObject currentComic = (ComicObject) req.getSession().getAttribute("currentComic");
		String newCoverSrc = req.getParameter("coverSrc");
		System.out.println(newCoverSrc);
		currentComic.setComicCover(newCoverSrc);
		ObjectifyService.ofy().save().entity(currentComic).now();
		resp.sendRedirect("/comicworkshopservlet");
		//req.getRequestDispatcher("Workshop_Comic_Series.jsp").forward(req, resp);
		//resp.sendRedirect("/loadworkshopajax");
		//req.getRequestDispatcher("/workshop").forward(req, resp);
	}
}
