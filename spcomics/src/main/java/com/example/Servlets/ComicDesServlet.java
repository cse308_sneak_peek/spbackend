package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicCommentObject;
import com.example.saveObj.ComicObject;

import com.example.saveObj.SubscriptionObject;
import com.example.saveObj.UserDatastore;

import com.example.saveObj.WorkshopObject;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 배요한 (Joan Bae) on 2016-04-12.
 */
public class ComicDesServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {    	
    	
		ComicObject comic = ObjectifyService.ofy()
		    				.load()
		    				.type(ComicObject.class)
		    				.filter("comicId", Long.parseLong(req.getParameter("comicId")))
		    				.first()
		    				.now();
    	
    	if(comic != null) {
    		List<ChapterObject> chapterList = ObjectifyService.ofy()
						    				.load()
						    				.type(ChapterObject.class)
						    				.ancestor(comic)
						    				//.ancestor(Key.create(ComicObject.class, comic.getComicId() ))
						    				.order("-chapterNum")
						    				.list();
			List<ChapterObject> chapterListtemp=new ArrayList<ChapterObject>();

			for(ChapterObject a : chapterList){
				if(a.getIsPublished()){
					chapterListtemp.add(a);
				}
			}

			chapterList=chapterListtemp;
    		
    		List<ComicCommentObject> commentList = ObjectifyService.ofy()
							    				.load()
							    				.type(ComicCommentObject.class)
							    				.ancestor(comic)
							    				//.ancestor(Key.create(ComicObject.class, comic.getComicId()))
							    				.order("-createdTime")
							    				.list();
    		
    		UserDatastore user = (UserDatastore)req.getSession().getAttribute("user_info");
    		
    		if(user != null) {
    			SubscriptionObject subscription = ObjectifyService.ofy()
						        				.load()
						        				.type(SubscriptionObject.class)
						        				.filter("userId", user.getUserId())
						        				.filter("comicId", comic.getComicId())
						        				.first()
						        				.now();
    			
    			if(subscription != null) {
    				req.setAttribute("isSubscribed", "true");
    			} else {
    				req.setAttribute("isSubscribed", "false");
    			}
    			
    		} else {
    			req.setAttribute("isSubscribed", "false");
    		}
    		
    		req.setAttribute("comicInfo", comic);
            req.setAttribute("chapterList", chapterList);
            req.setAttribute("commentList", commentList);
            
            req.getRequestDispatcher("/comicpage.jsp").forward(req, resp);
    	} else {
    		resp.sendRedirect("explore.html");
    	}
    	
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	
    	/* The case called by Commenting */
		ComicCommentObject comment;
		UserDatastore user = (UserDatastore)req.getSession().getAttribute("user_info");
        
    	String content = req.getParameter("content");
    	Long parentComicId = Long.parseLong(req.getParameter("comicId"));
    	String commentorName;
    	if(user != null) {
    		commentorName = user.getUserId();
    	} else {
    		commentorName = "default";
    	}
    	
    	comment = new ComicCommentObject(commentorName, content, parentComicId);
    	
    	ObjectifyService.ofy().save().entity(comment).now();
        
    	doGet(req, resp);
    }
    
}