package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicCommentObject;
import com.example.saveObj.ComicObject;
import com.example.saveObj.SubscriptionObject;
import com.example.saveObj.UserDatastore;
import com.example.saveObj.WorkshopObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.search.*;
import com.google.appengine.api.search.Document.Builder;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class ComicServlet extends HttpServlet{
	
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ComicObject comic = ObjectifyService.ofy()
				.load()
				.type(ComicObject.class)
				.filter("comicName", req.getParameter("comicName"))
				.first()
				.now();
		
		List<ChapterObject> chapterList = ObjectifyService.ofy()
				.load()
				.type(ChapterObject.class)
				.ancestor(comic)
				.list();
		
		List<ComicCommentObject> commentList = ObjectifyService.ofy()
				.load()
				.type(ComicCommentObject.class)
				.ancestor(comic)
				.list();
		
		req.setAttribute("comicInfo", comic);
        req.setAttribute("chapterList", chapterList);
        req.setAttribute("commentList", commentList);
		
		//req.getRequestDispatcher("/uploadComic.jsp").forward(req,resp);
    }

	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ComicObject comic;
		UserDatastore user = (UserDatastore)req.getSession().getAttribute("user_info");
		
		List<ComicObject> authorComics = ObjectifyService.ofy()
										.load()
										.type(ComicObject.class)
										.filter("authorName", user.getUserId())
										.list();
		
		String comicName = req.getParameter("comicName");
		boolean sameComicFlag = false;
		for(ComicObject comicObject : authorComics) {
			if(comicObject.getComicName().equals(comicName)) {
				System.out.println("   GIVEN COMIC NAME IS ALREADY EXISTED!!   ");
				sameComicFlag = true;
			}
		}
		
		if(!sameComicFlag) {
			String authorId = user.getUserId();
			String comicDescription = req.getParameter("comicDescription");
			String comicGenre = req.getParameter("comicGenre");
			comic = new ComicObject(authorId, comicName, comicDescription, comicGenre);
			
			/**
			 * TO DO: CONSTRUCT COMIC SERIES WITH TAGS NOT ADD THEM LIKE BELOW
			 */
			comic.addTag(req.getParameter("comicTag1"));
			comic.addTag(req.getParameter("comicTag2"));
			comic.addTag(req.getParameter("comicTag3"));
			
			ObjectifyService.ofy().save().entity(comic).now();
			
			createSearchableDoc("author", user.getId().toString(), authorId);
			createSearchableDoc("comic", comic.getComicId().toString(), comicName);
			
			req.setAttribute("comicId", comic.getComicId());
		}
		
		req.getRequestDispatcher("/uploadComic.jsp").forward(req, resp);
		
    }
	
	private void createSearchableDoc(String docType, String id, String searchedText) {
	    List<String> substrings = buildAllSubstrings(searchedText);
	    String combinedString = combine(substrings, " ");
	    // The input for this looks like "CHR CHRI CHRIS HRI HRIS" etc...
	    createDocument(docType, id, combinedString);
	}
	
	private List<String> buildAllSubstrings(String searchedText) {
	    List<String> substrings = new ArrayList<String>();
	    for (String word : searchedText.split(" ")) {
	        int wordSize = 1;
	        while (true) {
	            for (int i = 0; i < word.length() - wordSize + 1; i++) {
	                substrings.add(word.substring(i, i + wordSize));
	            }
	            if (wordSize == word.length())
	                break;
	            wordSize++;
	        }
	    }
	    return substrings;
	}

	private String combine(List<String> strings, String glue) {
	    int k = strings.size();
	    if (k == 0)
	        return null;
	    StringBuilder out = new StringBuilder();
	    out.append(strings.get(0));
	    for (int x = 1; x < k; ++x)
	        out.append(glue).append(strings.get(x));
	    return out.toString();
	}

	private void createDocument(String docType, String id, String searchableSubstring) {
	    Builder docBuilder = null;
	    if("author".equals(docType)) {
	    	docBuilder = Document
	    			.newBuilder()
		            .setId(id)
		            .addField(
		                    Field.newBuilder().setName("searchable_userId")
		                            .setText(searchableSubstring));
	    } else {
	    	docBuilder = Document
	    			.newBuilder()
		            .setId(id)
		            .addField(
		                    Field.newBuilder().setName("searchable_comicName")
		                            .setText(searchableSubstring));
	    }
	    
	    addDocToIndex(docBuilder.build(), docType);
	}
	
	private void addDocToIndex(Document document, String docType) {
	    Index index = getDocIndex(docType);
	   
	    try {
	        index.put(document);
	    } catch (PutException e) {
	        System.out.println("Error putting document in index... trying again.");
	        if (StatusCode.TRANSIENT_ERROR.equals(e.getOperationResult().getCode())) {
	            index.put(document);
	        }
	    }
	}

	public static Index getDocIndex(String docType) {
		IndexSpec indexSpec;
		if("author".equals(docType)) {
			indexSpec = IndexSpec.newBuilder().setName("USER_DOC_INDEX").build();
		} else {
			indexSpec = IndexSpec.newBuilder().setName("COMIC_DOC_INDEX").build();
		}
	    
	    Index index = SearchServiceFactory.getSearchService().getIndex(indexSpec);
	    return index;
	}

}
