package com.example.Servlets;

import com.example.saveObj.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.IndexSpec;
import com.google.appengine.api.search.PutException;
import com.google.appengine.api.search.SearchServiceFactory;
import com.google.appengine.api.search.StatusCode;
import com.google.appengine.api.search.Document.Builder;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.io.IOException;
import java.io.PrintWriter;



public class ComicWorkshopServlet extends HttpServlet{
	
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserDatastore user = (UserDatastore) req.getSession().getAttribute("user_info");
		if(user==null){

			resp.sendRedirect("/sign");
		}else{
            System.out.println("user name is "+user.getUserId());
			List<ComicObject>lst=ObjectifyService.ofy().load().type(ComicObject.class).filter("authorName",user.getUserId()).list();
            if(lst==null){
                lst=new ArrayList<ComicObject>();
            }
			req.getSession().setAttribute("lst",lst);
			//System.out.println("clickeddd");

			req.getRequestDispatcher("/workshop.jsp").forward(req,resp);
		}
    }

	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		UserDatastore user = (UserDatastore) req.getSession().getAttribute("user_info");
		if(user==null){
			resp.sendRedirect("/sign");
			//RequestDispatcher rd = req.getRequestDispatcher("Signin.java");
			//rd.forward(req, resp);
		}
		else{
			ComicObject comic;
			
			List<ComicObject> authorComics = ObjectifyService.ofy()
											.load()
											.type(ComicObject.class)
											.filter("authorName", user.getUserId())
											.list();
			
			String comicName = req.getParameter("csname");
			System.out.println(req.getParameter("tags"));
			boolean sameComicFlag = false;
			for(ComicObject comicObject : authorComics) {
				if(comicObject.getComicName().equals(comicName)) {
					System.out.println("   GIVEN COMIC NAME IS ALREADY EXISTED!!   ");
					sameComicFlag = true;
				}
			}
			
			if(!sameComicFlag) {
				String authorId = user.getUserId();
				String comicDescription = req.getParameter("description");
				String comicGenre = req.getParameter("Genre");
				String tags=req.getParameter("pass_tag");
				comic = new ComicObject(authorId, comicName, comicDescription, comicGenre);
				
				if(tags != "") {
					ArrayList<String> tt=new ArrayList<String>(Arrays.asList(tags.split("-")));
					comic.setTags(tt);
				}
				
				ObjectifyService.ofy().save().entity(comic).now();
				
				createSearchableDoc("author", user.getId().toString(), authorId);
				createSearchableDoc("comic", comic.getComicId().toString(), comicName);
				
				@SuppressWarnings("unchecked")
				List<ComicObject>lst=(List)(req.getSession().getAttribute("lst"));
				if(lst!=null){
					System.out.println("yealll");
				}else{
					System.out.println("noooo");
				}
				lst.add(comic);
				
				req.getSession().setAttribute("lst",lst);
			}
			
		}

		req.getRequestDispatcher("/workshop.jsp").forward(req,resp);
    }
	
	private void createSearchableDoc(String docType, String id, String searchedText) {
	    List<String> substrings = buildAllSubstrings(searchedText);
	    String combinedString = combine(substrings, " ");
	    createDocument(docType, id, combinedString);
	}
	
	private List<String> buildAllSubstrings(String searchedText) {
	    List<String> substrings = new ArrayList<String>();
	    for (String word : searchedText.split(" ")) {
	        int wordSize = 1;
	        while (true) {
	            for (int i = 0; i < word.length() - wordSize + 1; i++) {
	                substrings.add(word.substring(i, i + wordSize));
	            }
	            if (wordSize == word.length())
	                break;
	            wordSize++;
	        }
	    }
	    return substrings;
	}

	private String combine(List<String> strings, String glue) {
	    int k = strings.size();
	    if (k == 0)
	        return null;
	    StringBuilder out = new StringBuilder();
	    out.append(strings.get(0));
	    for (int x = 1; x < k; ++x)
	        out.append(glue).append(strings.get(x));
	    return out.toString();
	}

	private void createDocument(String docType, String id, String searchableSubstring) {
	    Builder docBuilder = null;
	    if("author".equals(docType)) {
	    	docBuilder = Document
	    			.newBuilder()
		            .setId(id)
		            .addField(
		                    Field.newBuilder().setName("searchable_userId")
		                            .setText(searchableSubstring));
	    } else {
	    	docBuilder = Document
	    			.newBuilder()
		            .setId(id)
		            .addField(
		                    Field.newBuilder().setName("searchable_comicName")
		                            .setText(searchableSubstring));
	    }
	    
	    addDocToIndex(docBuilder.build(), docType);
	}
	
	private void addDocToIndex(Document document, String docType) {
	    Index index = getDocIndex(docType);
	   
	    try {
	        index.put(document);
	    } catch (PutException e) {
	        System.out.println("Error putting document in index... trying again.");
	        if (StatusCode.TRANSIENT_ERROR.equals(e.getOperationResult().getCode())) {
	            index.put(document);
	        }
	    }
	}

	public static Index getDocIndex(String docType) {
		IndexSpec indexSpec;
		if("author".equals(docType)) {
			indexSpec = IndexSpec.newBuilder().setName("USER_DOC_INDEX").build();
		} else {
			indexSpec = IndexSpec.newBuilder().setName("COMIC_DOC_INDEX").build();
		}
	    
	    Index index = SearchServiceFactory.getSearchService().getIndex(indexSpec);
	    return index;
	}

}