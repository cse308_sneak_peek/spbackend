package com.example.Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.saveObj.ChapterObject;
import com.googlecode.objectify.ObjectifyService;

public class ExploreChapterServlet extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ChapterObject chapter;
		if(req.getParameter("chapterName")!=null){
			req.getSession().setAttribute("chapterName", req.getParameter("chapterName"));
		}
		String chapterName = (String) req.getSession().getAttribute("chapterName");
			chapter = ObjectifyService.ofy().load().type(ChapterObject.class)
				.filter("chapterName",chapterName).first()
				.now();
			List<String> currentChapterPages = chapter.getChapterPages();
			req.setAttribute("chapterInfo", chapter);
			req.setAttribute("currentChapterPages", currentChapterPages);
			req.getRequestDispatcher("/readPage.jsp").forward(req, resp);
	}
}
