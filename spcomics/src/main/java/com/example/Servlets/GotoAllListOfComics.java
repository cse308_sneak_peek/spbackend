package com.example.Servlets;

/**
 * Created by 배요한 (Joan Bae) on 2016-05-18.
 */

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicObject;
import com.example.spcomics.Comic;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class GotoAllListOfComics extends HttpServlet{

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<ComicObject> allcomics;
        List<ComicObject> A=null,B=null,C=null,D=null,E=null,F=null,G=null,H=null,I=null,J=null,K=null,L=null,M=null,N=null,O=null,P=null,Q=null,R=null
                ,S=null,T=null,U=null,V=null,W=null,X=null,Y=null,Z=null;


        allcomics = ObjectifyService.ofy()
                .load()
                .type(ComicObject.class)
                .order("-comicName")
                .list();

        for(ComicObject a:allcomics){
            char temp = a.getComicName().charAt(0);
            if(temp=='a' || temp=='A'){
                if(A==null){
                    A=new ArrayList<ComicObject>();
                }
                A.add(a);
            }else if(temp=='b' || temp=='B'){
                if(B==null){
                    B=new ArrayList<ComicObject>();
                }
                B.add(a);

            }else if(temp=='c' || temp=='C'){
                if(C==null){
                    C=new ArrayList<ComicObject>();
                }
                C.add(a);

            }else if(temp=='d' || temp=='D'){
                if(D==null){
                    D=new ArrayList<ComicObject>();
                }
                D.add(a);

            }else if(temp=='e' || temp=='E'){
                if(E==null){
                    E=new ArrayList<ComicObject>();
                }
                E.add(a);

            }else if(temp=='F' || temp=='f'){
                if(F==null){
                    F=new ArrayList<ComicObject>();
                }
                F.add(a);

            }else if(temp=='G' || temp=='g'){
                if(G==null){
                    G=new ArrayList<ComicObject>();
                }
                G.add(a);

            }else if(temp=='H' || temp=='h'){
                if(H==null){
                    H=new ArrayList<ComicObject>();
                }
                H.add(a);

            }else if(temp=='i' || temp=='I'){
                if(I==null){
                    I=new ArrayList<ComicObject>();
                }
                I.add(a);

            }else if(temp=='J' || temp=='j'){
                if(J==null){
                    J=new ArrayList<ComicObject>();
                }
                J.add(a);

            }else if(temp=='K' || temp=='k'){
                if(K==null){
                    K=new ArrayList<ComicObject>();
                }
                K.add(a);

            }else if(temp=='L' || temp=='l'){
                if(L==null){
                    L=new ArrayList<ComicObject>();
                }
                L.add(a);

            }else if(temp=='m' || temp=='M'){
                if(M==null){
                    M=new ArrayList<ComicObject>();
                }
                M.add(a);

            }else if(temp=='n' || temp=='N'){
                if(N==null){
                    N=new ArrayList<ComicObject>();
                }
                N.add(a);

            }else if(temp=='o' || temp=='O'){
                if(O==null){
                    O=new ArrayList<ComicObject>();
                }
                O.add(a);

            }else if(temp=='p' || temp=='P'){
                if(P==null){
                    P=new ArrayList<ComicObject>();
                }
                P.add(a);

            }else if(temp=='q' || temp=='Q'){
                if(Q==null){
                    Q=new ArrayList<ComicObject>();
                }
                Q.add(a);

            }else if(temp=='r' || temp=='R'){
                if(R==null){
                    R=new ArrayList<ComicObject>();
                }
                R.add(a);

            }else if(temp=='s' || temp=='S'){
                if(S==null){
                    S=new ArrayList<ComicObject>();
                }
                S.add(a);

            }else if(temp=='t' || temp=='T'){
                if(T==null){
                    T=new ArrayList<ComicObject>();
                }
                T.add(a);

            }else if(temp=='u' || temp=='U'){
                if(U==null){
                    U=new ArrayList<ComicObject>();
                }
                U.add(a);

            }else if(temp=='v' || temp=='V'){
                if(V==null){
                    V=new ArrayList<ComicObject>();
                }
                V.add(a);

            }else if(temp=='w' || temp=='W'){
                if(W==null){
                    W=new ArrayList<ComicObject>();
                }
                W.add(a);

            }else if(temp=='x' || temp=='X'){
                if(X==null){
                    X=new ArrayList<ComicObject>();
                }
                X.add(a);

            }else if(temp=='y' || temp=='Y'){
                if(Y==null){
                    Y=new ArrayList<ComicObject>();
                }
                Y.add(a);

            }else if(temp=='z' || temp=='Z'){
                if(Z==null){
                    Z=new ArrayList<ComicObject>();
                }
                Z.add(a);

            }
        }





        req.getSession().setAttribute("A",A);
        req.getSession().setAttribute("B",B);
        req.getSession().setAttribute("C",C);
        req.getSession().setAttribute("D",D);
        req.getSession().setAttribute("E",E);
        req.getSession().setAttribute("F",F);
        req.getSession().setAttribute("G",G);
        req.getSession().setAttribute("H",H);
        req.getSession().setAttribute("I",I);
        req.getSession().setAttribute("J",J);
        req.getSession().setAttribute("K",K);
        req.getSession().setAttribute("L",L);
        req.getSession().setAttribute("M",M);
        req.getSession().setAttribute("N",N);
        req.getSession().setAttribute("O",O);
        req.getSession().setAttribute("P",P);
        req.getSession().setAttribute("Q",Q);
        req.getSession().setAttribute("R",R);
        req.getSession().setAttribute("S",S);
        req.getSession().setAttribute("T",T);
        req.getSession().setAttribute("U",U);
        req.getSession().setAttribute("V",V);
        req.getSession().setAttribute("W",W);
        req.getSession().setAttribute("X",X);
        req.getSession().setAttribute("Y",Y);
        req.getSession().setAttribute("Z",Z);

        req.getRequestDispatcher("AllListOfComics.jsp").forward(req,resp);



    }
}
