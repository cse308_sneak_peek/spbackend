package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicObject;
import com.example.saveObj.HistoryObject;
import com.example.saveObj.UserDatastore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.io.IOException;

public class HistoryServlet extends HttpServlet{
	
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserDatastore user = (UserDatastore)req.getSession().getAttribute("user_info");
		
		if(user != null) {
			String userId = user.getUserId();
			
			List<HistoryObject> userHistoryList = ObjectifyService.ofy()
											.load()
											.type(HistoryObject.class)
											.filter("userId", userId)
											.order("-viewedDate")
											.list();
			
			ArrayList<Date> viewedDateList = new ArrayList<Date>();
			ArrayList<HistoryObject> historySameDate = new ArrayList<HistoryObject>();
			ArrayList<ArrayList<HistoryObject>> historyList = new ArrayList<ArrayList<HistoryObject>>();
			boolean first = true;
			for(HistoryObject userHistory : userHistoryList) {
				Date viewedDate = userHistory.getViewedDate();
				
				if(first) {
					viewedDateList.add(viewedDate);
					historySameDate.add(userHistory);
					first = false;
				} else {
					Calendar viewedCal = Calendar.getInstance();
				    viewedCal.setTime(viewedDate);
				    int viewedMonth = viewedCal.get(Calendar.MONTH);
				    int viewedDay = viewedCal.get(Calendar.DAY_OF_MONTH);
				    
					Date lastElem = viewedDateList.get(viewedDateList.size()-1);
					Calendar lastElemCal = Calendar.getInstance();
					lastElemCal.setTime(lastElem);
				    int lastElemMonth = lastElemCal.get(Calendar.MONTH);
				    int lastElemDay = lastElemCal.get(Calendar.DAY_OF_MONTH);
				    
				    if(viewedMonth == lastElemMonth && viewedDay == lastElemDay) {
				    	historySameDate.add(userHistory);
				    } else {
				    	System.out.println(" viewedDate is different with lastElemDate ");
				    	// add new date into list
				    	historyList.add(historySameDate);
				    	historySameDate.clear();
				    	viewedDateList.add(viewedDate);
				    	historySameDate.add(userHistory);
				    }
				}
			}
			historyList.add(historySameDate);
			
			req.setAttribute("viewedDateList", viewedDateList);
			req.setAttribute("historyList", historyList);
			
			req.getRequestDispatcher("/history.jsp").forward(req,resp);
			
		} else {
			resp.sendRedirect("/sign");
		}
		
    }
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
    }

}