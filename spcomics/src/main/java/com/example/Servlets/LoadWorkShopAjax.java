package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicObject;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.saveObj.ComicObject;
import com.googlecode.objectify.ObjectifyService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by 배요한 (Joan Bae) on 2016-05-04.
 */
public class LoadWorkShopAjax extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input=req.getParameter("input");
        input=input.replaceAll("_"," ");
        req.setAttribute("csName",input);
        ComicObject comic = ObjectifyService
        		.ofy()
        		.load()
        		.type(ComicObject.class)
        		.filter("comicName", input)
        		.first().now();

        List<ComicObject> lst= ObjectifyService.ofy().load().type(ComicObject.class).filter("comicName",input).list();
        ComicObject obj=lst.get(0);

        List<ChapterObject> chapterList = ObjectifyService.ofy()
                .load()
                .type(ChapterObject.class)
                .ancestor(comic)
                .ancestor(Key.create(ComicObject.class, comic.getComicId() ))
                .order("-chapterNum")
                .list();

        req.getSession().setAttribute("currentComic", comic); //yao added
        req.getSession().setAttribute("listofComicChapters",chapterList);
        req.setAttribute("cs",obj);
        req.getRequestDispatcher("Workshop_Comic_Series.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("hellow hi you");
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try{
            String s2=request.getParameter("input");
            if(s2==""){

                out.println("nothing");

            }else{
                out.println(s2);
            }

        }catch(Exception i){

        }

    }
}
