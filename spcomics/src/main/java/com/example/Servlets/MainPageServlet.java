package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicObject;
import com.example.spcomics.Comic;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by 배요한 (Joan Bae) on 2016-05-10.
 */
public class MainPageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ComicObject comic;
        List<ComicObject> latestSeries;
        List<ChapterObject> latestChapter= new ArrayList<ChapterObject>();
        List<ComicObject> latestChapterForComicObject;
        List<ComicObject> recommandBySubscribed;


        latestSeries = ObjectifyService.ofy()
                .load()
                .type(ComicObject.class)
                .order("-createdTime")
                .limit(8)
                .list();

        List<ComicObject> allComics=ObjectifyService.ofy()
                .load()
                .type(ComicObject.class)
                .order("-comicName")
                .list();

        latestChapterForComicObject=ObjectifyService.ofy().load().type(ComicObject.class).list();
        recommandBySubscribed=ObjectifyService.ofy().load().type(ComicObject.class).order("-numSubscribed").limit(8).list();

        for(ComicObject a:latestChapterForComicObject){

            if(a.getLatestChaptername()!=""){
                ChapterObject chapter = ObjectifyService.ofy()
                        .load()
                        .type(ChapterObject.class)
                        .ancestor(a)
                        .filter("chapterName", a.getLatestChaptername())
                        .first()
                        .now();

                chapter.setSeriesName(a.getComicName());
                chapter.setComicCover(a.getComicCover());
                latestChapter.add(chapter);

            }


        }



        Collections.sort(latestChapter,new Comparator<ChapterObject>(){
           public int compare(ChapterObject a, ChapterObject b){
               return b.getCreatedTime().compareTo(a.getCreatedTime());
           }
        });

        if(latestChapter.size()>8){
            latestChapter= latestChapter.subList(0,8);
        }




        req.getSession().setAttribute("latestSeries",latestSeries);
        req.getSession().setAttribute("latestChapter",latestChapter);
        req.getSession().setAttribute("recommandBySubscribed",recommandBySubscribed);
        //System.out.println("heloooooooo");
        req.getRequestDispatcher("sidebar.jsp").forward(req,resp);



    }
}
