package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicObject;
import com.example.saveObj.SubscriptionObject;
import com.example.saveObj.UserDatastore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class NewChapterServlet extends HttpServlet{
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("I AM CALLED, MOTHERFKERS");
		int chapterNum = Integer.parseInt(req.getParameter("chapter_num"));
		String chapterName = req.getParameter("chapter_name");
		Long comicId = Long.parseLong(req.getParameter("comicId"));
		ChapterObject newChapter = new ChapterObject(chapterNum,chapterName,comicId);
		ComicObject currentComic = (ComicObject) req.getSession().getAttribute("currentComic");
		currentComic.setNumOfChapters(currentComic.getNumOfChapters()+1);
		ObjectifyService.ofy().save().entity(currentComic).now();
		ObjectifyService.ofy().save().entity(newChapter).now();
		req.getSession().setAttribute("chapterNumber", chapterNum);
		req.getSession().setAttribute("currentChapter", newChapter);
		System.out.println(((ChapterObject)req.getSession().getAttribute("currentChapter")).getChapterName());
		//req.getSession().setAttribute("WS_RELATED", "NO");
		resp.sendRedirect("/workshop");
		//req.getRequestDispatcher("/workshop").forward(req, resp);
	}
}