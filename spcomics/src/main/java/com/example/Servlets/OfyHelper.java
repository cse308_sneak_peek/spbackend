package com.example.Servlets;

import com.example.saveObj.*;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by 배요한 (Joan Bae) on 2016-02-11.
 */
public class OfyHelper implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ObjectifyService.register(WorkshopObject.class);        
        ObjectifyService.register(ComicObject.class);
        ObjectifyService.register(ChapterObject.class);
        ObjectifyService.register(ComicCommentObject.class);
        ObjectifyService.register(UserDatastore.class);
        ObjectifyService.register(HistoryObject.class);
        ObjectifyService.register(SubscriptionObject.class);
        ObjectifyService.register(ReadingCommentObject.class);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}