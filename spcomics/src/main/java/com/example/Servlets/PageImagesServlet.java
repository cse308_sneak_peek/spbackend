package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicObject;
import com.example.saveObj.SubscriptionObject;
import com.example.saveObj.UserDatastore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class PageImagesServlet extends HttpServlet{
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ChapterObject chapter = (ChapterObject) req.getSession().getAttribute("currentChapter");
		int index = Integer.parseInt(req.getParameter("updateIndex"));
		String updateImage = req.getParameter("updatePage");
		String updateImageLoad = req.getParameter("updatePageLoad");
		chapter.changePage(index,updateImage);
		chapter.changePageLoad(index,updateImageLoad);
		ObjectifyService.ofy().save().entity(chapter).now();
		resp.sendRedirect("/workshop");
		//req.getRequestDispatcher("/workshop").forward(req, resp);
	}
}