package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicObject;
import com.example.saveObj.SubscriptionObject;
import com.example.saveObj.UserDatastore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class PageUpdateServlet extends HttpServlet{
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//ChapterObject chapter = (ChapterObject) req.getSession().getAttribute("currentChapter");
		int index = Integer.parseInt(req.getParameter("currentIndex"));
		req.getSession().setAttribute("currentIndex", index);
		System.out.println(req.getSession().getAttribute("currentIndex"));
		resp.sendRedirect("/workshop");
		//req.getRequestDispatcher("/workshop").forward(req, resp);
	}
}