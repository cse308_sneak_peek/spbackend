package com.example.Servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicObject;
import com.example.saveObj.WorkshopObject;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;





import java.util.ArrayList;
import java.util.List;


public class PublishChapterServlet extends HttpServlet{
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ComicObject comic = (ComicObject) req.getSession().getAttribute("currentComic");
		ChapterObject chapter = ObjectifyService
				.ofy()
				.load()
				.type(ChapterObject.class)
				.ancestor(comic)
				.filter("chapterNum",
						Integer.parseInt(req.getParameter("publish_num")))
				.first().now();
		chapter.setPublished(true);
		comic.setLatestChaptername(chapter.getChapterName());
		ObjectifyService.ofy().save().entity(chapter).now();
		ObjectifyService.ofy().save().entity(comic).now();
        resp.sendRedirect("/comicworkshopservlet");
		//req.getRequestDispatcher("/comicworkshopservlet").forward(req,resp);
	}
}