package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicCommentObject;
import com.example.saveObj.ComicObject;
import com.example.saveObj.HistoryObject;
import com.example.saveObj.ReadingCommentObject;
import com.example.saveObj.SubscriptionObject;
import com.example.saveObj.UserDatastore;
import com.example.saveObj.WorkshopObject;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

public class ReadingCommentServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		UserDatastore user = (UserDatastore)req.getSession().getAttribute("user_info");
		if(req.getParameter("comicId")!=null){
			req.getSession().setAttribute("comicId", req.getParameter("comicId"));
		}
		if(req.getParameter("chapterNum")!=null){
			req.getSession().setAttribute("chapterNum", req.getParameter("chapterNum"));
		}
		ComicObject comic = ObjectifyService
							.ofy()
							.load()
							.type(ComicObject.class)
							.filter("comicId", Long.parseLong((String)req.getSession().getAttribute("comicId")))
							.first()
							.now();
		if(comic!=null){
			req.setAttribute("comicInfo", comic);
			req.getSession().setAttribute("currentComic", comic);
		}
		ChapterObject chapter = ObjectifyService
								.ofy()
								.load()
								.type(ChapterObject.class)
								.ancestor(comic)
								.filter("chapterNum", Integer.parseInt((String)req.getSession().getAttribute("chapterNum")))
								.first()
								.now();
		
		Key<ChapterObject> key = Key.create(ChapterObject.class, chapter.getChapterId().longValue());
		List<String> currentChapterPages = chapter.getChapterPages();
		List<ReadingCommentObject> commentList = ObjectifyService
				.ofy()
				.load()
				.type(ReadingCommentObject.class)
				.ancestor(key)
				.order("-createdTime")
				.list();
		
		if(user != null) {
			keepHistory(user.getUserId(), comic.getComicId().longValue(), comic.getComicName(), chapter.getChapterNum(), chapter.getChapterName());
		}
		
		req.setAttribute("chapterInfo", chapter);
		req.setAttribute("currentChapterPages", currentChapterPages);
		req.setAttribute("commentList", commentList);
		req.setAttribute("chapterNum", chapter.getChapterNum());
		req.setAttribute("comicId", comic.getComicId());
		req.getRequestDispatcher("/readPage.jsp").forward(req, resp);

	}
	
	public void keepHistory(String userId, Long comicId, String comicName, int chapterNum, String chapterName) {
		/* keep the reading history */
		System.out.println("parameters in keepHistory: "+userId+", "+comicId+", "+chapterNum);
		List<HistoryObject> userHistories = ObjectifyService.ofy()
											.load()
											.type(HistoryObject.class)
											.filter("userId", userId)
											.list();

		HistoryObject history=null;
		for(HistoryObject userHistory : userHistories) {
			if((comicId == userHistory.getComicId().longValue()) && (chapterNum == userHistory.getChapterNum())) {
				history = userHistory;
				break;
			}
		}

		if(history == null) {
			history = new HistoryObject(userId, comicId, comicName, chapterNum, chapterName);
		} else {
			history.setViewedDate();
		}
		ObjectifyService.ofy().save().entity(history).now();

		return;
	}

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	
    	/* The case called by Commenting */
		ReadingCommentObject comment;
		UserDatastore user = (UserDatastore)req.getSession().getAttribute("user_info");
        
    	String content = req.getParameter("content");
    	Long chapterId = Long.parseLong(req.getParameter("chapterId"));
    	String commentorName;
    	if(user != null) {
    		commentorName = user.getUserId();
    	} else {
    		commentorName = "default";
    	}
    	Key<ChapterObject> key = Key.create(ChapterObject.class, chapterId);
    	
    	comment = new ReadingCommentObject(commentorName, content, key, chapterId);
    	
    	ObjectifyService.ofy().save().entity(comment).now();
        doGet(req, resp);
    }
    
}