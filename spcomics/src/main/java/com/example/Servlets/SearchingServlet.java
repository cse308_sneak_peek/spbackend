package com.example.Servlets;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicCommentObject;
import com.example.saveObj.ComicObject;
import com.example.saveObj.UserDatastore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.IndexSpec;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SearchServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class SearchingServlet extends HttpServlet{
	
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if("true".equals(req.getParameter("byTag"))) {
			/* Searching by Tag */
			String keyword = req.getParameter("keyword");
			
			List<ComicObject> searchingResult = ObjectifyService.ofy()
												.load()
												.type(ComicObject.class)
												.filter("tags", keyword)
												.list();
			
			req.setAttribute("searchingResult", searchingResult);
			req.setAttribute("inputKeyword", keyword);
			
			req.getRequestDispatcher("/searchResult.jsp").forward(req,resp);
			
		} else {
			/* Searching by keywords */
			// Searching author ID
			Query queryForAuthorId = Query.newBuilder().build("searchable_userId" + "=" + req.getParameter("keyword"));
		    Index userDocIndex = getDocIndex("user");
		    Results<ScoredDocument> matchingResults = userDocIndex.search(queryForAuthorId);
		    
		    List<Long> keyOfMatchingResults = new ArrayList<Long>();
		    for (ScoredDocument document : matchingResults) {
		    	int i = 1;
		    	System.out.println(" searchable_userId [#" + i +"]");
		    	i++;
		    	keyOfMatchingResults.add(Long.parseLong(document.getId()));		    	
		    }
		    
		    List<UserDatastore> authorResult = new ArrayList<UserDatastore>();
		    for(Long key : keyOfMatchingResults) {
		    	UserDatastore user = ObjectifyService.ofy()
									.load()
									.type(UserDatastore.class)
									.filter("id", key)
									.first()
									.now();
				authorResult.add(user);		
		    }
		    
		    // Searching comic series name
		    Query queryForComicName = Query.newBuilder().build("searchable_comicName" + "=" + req.getParameter("keyword"));
		    userDocIndex = getDocIndex("comic");
		    matchingResults = userDocIndex.search(queryForComicName);
		    
		    keyOfMatchingResults = new ArrayList<Long>();
		    for (ScoredDocument document : matchingResults) {
		    	int i = 1;
		    	System.out.println(" searchable_comicName [#" + i +"]");
		    	i++;
		    	keyOfMatchingResults.add(Long.parseLong(document.getId()));		    	
		    }
		    
		    List<ComicObject> comicResult = new ArrayList<ComicObject>();
		    for(Long key : keyOfMatchingResults) {
		    	ComicObject comic = ObjectifyService.ofy()
									.load()
									.type(ComicObject.class)
									.filter("comicId", key)
									.first()
									.now();
				comicResult.add(comic);		
		    }
		    
		    System.out.println(" authorResult num: "+authorResult.size()+",  comicResult num: "+comicResult.size());
		    req.setAttribute("authorResult", authorResult);
		    req.setAttribute("comicResult", comicResult);
		    req.setAttribute("inputKeyword", req.getParameter("keyword"));
		    
		    req.getRequestDispatcher("/search.jsp").forward(req,resp);
		}
		
    }
	
	public static Index getDocIndex(String docType) {
		IndexSpec indexSpec;
		if("user".equals(docType)) {
			indexSpec = IndexSpec.newBuilder().setName("USER_DOC_INDEX").build();
		} else {
			indexSpec = IndexSpec.newBuilder().setName("COMIC_DOC_INDEX").build();
		}
	    
	    Index index = SearchServiceFactory.getSearchService().getIndex(indexSpec);
	    return index;
	}

	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
    }

}