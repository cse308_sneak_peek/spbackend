package com.example.Servlets;

import com.example.saveObj.ComicObject;
import com.example.saveObj.UserDatastore;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by 배요한 (Joan Bae) on 2016-04-19.
 */
public class Signin extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //System.out.println("hello");
        UserDatastore user= (UserDatastore) (req.getSession().getAttribute("user_info"));
        String user_id=user.getUserId();
        UserDatastore already=ofy().load().type(UserDatastore.class).filter("userId",user_id).first().now();
        already.setNickname(req.getParameter("change_nick"));
        req.setAttribute("warning_nickname","Your new nickname will be reflected next time when you log in. ");
        ofy().save().entity(already).now();


        String author = user.getUserId();
        List<ComicObject> comics = ObjectifyService.ofy()
                .load()
                .type(ComicObject.class)
                .filter("authorName", author)
                .list();

        for(ComicObject a:comics){
            System.out.println("the name is "+a.getComicName());
        }
        req.getSession().setAttribute("authorName1",author);
        req.getSession().setAttribute("authorcomics",comics);


        req.getSession().setAttribute("user_info",already);
        req.getRequestDispatcher("/profile.jsp").forward(req,resp);


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //   resp.addHeader("X-Frame-Options", "SAMEORIGIN");

        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();



        resp.setContentType("text/html");

        if (user != null) {
            if(req.getSession().getAttribute("user_info")!=null){
                //System.out.println("1. user is not null");

                String author = user.getUserId();
                List<ComicObject> comics = ObjectifyService.ofy()
                        .load()
                        .type(ComicObject.class)
                        .filter("authorName", author)
                        .list();


                req.getSession().setAttribute("authorName1",author);
                req.getSession().setAttribute("authorcomics1",comics);


                req.getSession().setAttribute("path1","profile.jsp");
                req.getRequestDispatcher("/sidebar.jsp").forward(req,resp);
            }else{
                //System.out.println("2. user is null");
                String userId=(user.getEmail()).split("@")[0];
                UserDatastore already=ofy().load().type(UserDatastore.class).filter("userId",userId).first().now();

                if(already==null){

                    already=new UserDatastore(userId);
                    ofy().save().entity(already).now();
                }else{
                    //System.out.println("3.but the suer exists");
                }



                req.getSession().setAttribute("user_info",already);
                req.getSession().setAttribute("path1","profile.jsp");
                //System.out.println("---------------------");
                req.getRequestDispatcher("/sidebar.jsp").forward(req,resp);
            }


        } else {
            resp.setContentType("text/html");
            resp.getWriter().println(
                    "Please <a href='"
                            + userService.createLoginURL(req.getRequestURI())
                            + "'> LogIn </a>");
        }

    }
}
