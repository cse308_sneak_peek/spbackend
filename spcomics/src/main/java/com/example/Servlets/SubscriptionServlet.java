package com.example.Servlets;

import com.example.saveObj.ComicObject;
import com.example.saveObj.SubscriptionObject;
import com.example.saveObj.UserDatastore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class SubscriptionServlet extends HttpServlet{
	
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserDatastore user = (UserDatastore)req.getSession().getAttribute("user_info");
		
		if(user != null) {
			String userId = user.getUserId();
			
			List<SubscriptionObject> subscriptionList = ObjectifyService.ofy()
					.load()
					.type(SubscriptionObject.class)
					.filter("userId", userId)
					.list();
			
			List<ComicObject> comicList = new ArrayList<ComicObject>();
			for(SubscriptionObject subscription : subscriptionList) {
				ComicObject comic = ObjectifyService.ofy()
	    				.load()
	    				.type(ComicObject.class)
	    				.filter("comicId", subscription.getComicId())
	    				.first()
	    				.now();
				
				comicList.add(comic);
			}
			
			req.setAttribute("subscription", subscriptionList);
			req.setAttribute("comicInfo", comicList);
			
			req.getRequestDispatcher("/subscribed.jsp").forward(req,resp);
			
		} else {
			resp.sendRedirect("/sign");
		}
		
    }

	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		UserDatastore user = (UserDatastore)req.getSession().getAttribute("user_info");
		
		if(user != null) {
			if("Subscribe".equals(req.getParameter("Subscribe"))) {	    		
	    		ComicObject comic = ObjectifyService.ofy()
				    				.load()
				    				.type(ComicObject.class)
				    				.filter("comicId", Long.parseLong(req.getParameter("comicId")))
				    				.first()
				    				.now();
	    		
	    		comic.setNumSubscribed(1);
	    		ObjectifyService.ofy().save().entity(comic).now();
	    		
	    		SubscriptionObject subscription = new SubscriptionObject(user.getUserId(), Long.parseLong(req.getParameter("comicId")));
	    		ObjectifyService.ofy().save().entity(subscription).now();
	    		
	    	} else if("Unsubscribe".equals(req.getParameter("Subscribe"))) {	    		
	    		ComicObject comic = ObjectifyService.ofy()
				    				.load()
				    				.type(ComicObject.class)
				    				.filter("comicId", Long.parseLong(req.getParameter("comicId")))
				    				.first()
				    				.now();
				
				comic.setNumSubscribed(-1);
				ObjectifyService.ofy().save().entity(comic).now();
				
				SubscriptionObject subscription = ObjectifyService.ofy()
							    				.load()
							    				.type(SubscriptionObject.class)
							    				.filter("userId", user.getUserId())
							    				.filter("comicId", comic.getComicId())
							    				.first()
							    				.now();
				ObjectifyService.ofy().delete().entity(subscription).now();
				
	    	}
			
			req.getRequestDispatcher("/comicpage.jsp").forward(req, resp);
			
		} else {
			
		}
		
    }

}