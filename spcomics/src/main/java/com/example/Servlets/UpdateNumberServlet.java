package com.example.Servlets;

import com.example.saveObj.ComicObject;
import com.example.saveObj.SubscriptionObject;
import com.example.saveObj.UserDatastore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class UpdateNumberServlet extends HttpServlet{
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().setAttribute("currentPageNumber", req.getParameter("pageNumber"));
		req.getRequestDispatcher("workzoom.jsp").forward(req, resp);
	}
}
