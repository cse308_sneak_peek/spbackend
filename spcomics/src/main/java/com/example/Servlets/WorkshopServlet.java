package com.example.Servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.saveObj.ChapterObject;
import com.example.saveObj.ComicObject;
import com.example.saveObj.WorkshopObject;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.googlecode.objectify.ObjectifyService;




import java.util.ArrayList;
import java.util.List;


public class WorkshopServlet extends HttpServlet{

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();  // Find out who the user is.
        ComicObject currentComic = (ComicObject) req.getSession().getAttribute("currentComic");
        int chapterNum;
		ChapterObject current;
		List<String> chapterStrings;
		List<String> chapterLoadStrings;
        if(currentComic.getNumOfChapters()==0){
        	//req.getRequestDispatcher("attemptAccessWS.jsp").forward(req,resp);
        	/*
        	 * No chapters yet, attempting to access workshop.
        	 * currentChapter = null;
			 * chapterStrings = null;
			 * chapterLoadStrings = null;
        	 * 
        	 * 
        	 */
        }
        else{
        	if (req.getParameter("chapterNumber") != null) {
    			chapterNum = Integer.parseInt(req.getParameter("chapterNumber"));
    	        req.getSession().setAttribute("chapterNumber", chapterNum);
    		}
    		else{
    			/*
    			if( req.getSession().getAttribute("chapterNumber")==null){
    				System.out.println("SHIT");
    				chapterNum = 1;
    			}
    			else{
    			*/
    				chapterNum = (int) req.getSession().getAttribute("chapterNumber");
    				ChapterObject currentChapter = (ChapterObject) req.getSession().getAttribute("currentChapter");
    				System.out.println("NOTICE ME, CHAPTERNUMBER SENPAI " + chapterNum);
    				System.out.println("NOTICE ME, CHAPTER SENPAI " + currentChapter);
    			//}
    		}
			System.out.println("Chapter num is " + chapterNum);
			List<ChapterObject> chapterList = (List<ChapterObject>) req
					.getSession().getAttribute("listofComicChapters");
			current = null;
			chapterStrings = null;
			chapterLoadStrings = null;
			for (ChapterObject a : chapterList) {
				if (a.getChapterNum() == chapterNum) {
					current = a;
					chapterLoadStrings = a.getChapterPagesLoad();
					break;
				}
			}
			for (ChapterObject a : chapterList) {
				if (a.getChapterNum() == chapterNum) {
					current = a;
					chapterStrings = a.getChapterPages();
			        req.getSession().setAttribute("currentChapter", current);
					break;
				}
			}
	        req.getSession().setAttribute("PASSING", "B");
	        req.setAttribute("currentChapterPagesLoad", chapterLoadStrings);
	        req.setAttribute("currentChapterPages",chapterStrings);
        }
        /*
         *Sets the workshop objects for the user 
         */
        List<WorkshopObject> workshoplist = ObjectifyService.ofy().load()
				.type(WorkshopObject.class).list();
        req.setAttribute("workshoplist",workshoplist);
        /*
        for(WorkshopObject a :workshoplist){
        	if(a.getUserId().equals(user.getEmail().split("@")[0])){
                req.setAttribute("a",a);
            }
        }
//        */
//        for(WorkshopObject item : workshoplist){
//        	if(req.getParameter("name").equals(item.getName())||req.getParameter("name")!=null){
//        		req.setAttribute("outputImage", item.getValue());
//        	}
//        }
        req.getRequestDispatcher("/workzoom.jsp").forward(req,resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        WorkshopObject workshop;
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();  // Find out who the user is.
        String name = req.getParameter("userId");
        String value = req.getParameter("content");
        String type = req.getParameter("type");
        String objectName = req.getParameter("name");
        System.out.println("objectname is "+objectName);
        String secondaryValue = req.getParameter("secondaryValue");
        List<WorkshopObject> workshoplist = ObjectifyService.ofy()
                .load()
                .type(WorkshopObject.class)
                .list();
        workshop = new WorkshopObject(name,value,secondaryValue,type,objectName);
        /*
         * 
         */
        	ObjectifyService.ofy().save().entity(workshop).now();
        	
        	req.setAttribute("workshoplist", workshoplist);
        	//req.getSession().setAttribute("workshoplist", workshoplist);
        	req.getRequestDispatcher("/workzoom.jsp").forward(req,resp);
    }
}
