package com.example.saveObj;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class ChapterObject implements Serializable{
	@Parent Key<ComicObject> comic;
	@Id private Long chapterId;
	private String comicCover;
	@Index private int chapterNum;
	@Index private String chapterName;
	private Date createdTime;
	private List<String> chapterPagesDisplay;
	private List<String> chapterPagesLoad;
	private int numOfPages;
	private boolean isPublished;
	private String seriesName;
	
    public ChapterObject(){
    	
    }
    /*
    public ChapterObject (String chapterName, Long parentComicId){
    	setChapterNum(1);
    	this.chapterName = chapterName;
        this.comic = Key.create(ComicObject.class, parentComicId);
        this.createdTime = new Date();
        chapterPagesDisplay = new ArrayList<String>();
        chapterPagesLoad = new ArrayList<String>();
        setNumOfPages(0);
        isPublished = false;
    }
    */
    public ChapterObject(int chapterNum, String chapterName, Long parentComicId){
    	this.chapterNum = chapterNum;
        this.chapterName = chapterName;
        this.comic = Key.create(ComicObject.class, parentComicId);
        this.createdTime = new Date();
        chapterPagesDisplay = new ArrayList<String>();
        chapterPagesLoad = new ArrayList<String>();
        setNumOfPages(0);
        isPublished = false;
    }
	public void changePageLoad(int index, String updateImageLoad) {
		chapterPagesLoad.set(index, updateImageLoad);
	}
    public void changePage(int index, String page){
    	chapterPagesDisplay.set(index, page);
    }
    public void addChapterPageLoad(String page){
    	chapterPagesLoad.add(page);
    }
    public void addChapterPage(String page){
    	chapterPagesDisplay.add(page);
    }
	public List<String> getChapterPages() {
		return chapterPagesDisplay;
	}

	public void setChapterPages(List<String> chapterPages) {
		this.chapterPagesDisplay = chapterPages;
	}

	public boolean getIsPublished() {
		return isPublished;
	}


	public void setPublished(boolean isPublished) {
		this.isPublished = isPublished;
	}
	
    public Long getChapterId() {
    	return chapterId;
    }
    
    public int getChapterNum() {
    	return chapterNum;
    }
    public void setChapterNum(int chapterNum) {
    	this.chapterNum = chapterNum;
    }
    
    public String getChapterName() {
    	return chapterName;
    }
    public void setChapterName(String comicName) {
    	this.chapterName = comicName;
    }
    
    public Date getCreatedTime() {
    	return createdTime;
    }
    public void setCreatedTime(Date createdTime){
    	this.createdTime = createdTime;
    }

	public int getNumOfPages() {
		return numOfPages;
	}

	public void setNumOfPages(int numOfPages) {
		this.numOfPages = numOfPages;
	}

	public List<String> getChapterPagesLoad() {
		return chapterPagesLoad;
	}

	public void setChapterPagesLoad(List<String> chapterPagesLoad) {
		this.chapterPagesLoad = chapterPagesLoad;
	}

	public String getSeriesName() {
		return seriesName;
	}

	public void setSeriesName(String seriesName) {
		this.seriesName = seriesName;
	}
	public String getComicCover() {
		return comicCover;
	}
	public void setComicCover(String comicCover) {
		this.comicCover = comicCover;
	}
}
