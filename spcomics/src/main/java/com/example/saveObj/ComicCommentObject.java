package com.example.saveObj;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by 배요한 (Joan Bae) on 2016-04-12.
 */
@Entity
public class ComicCommentObject {
    @Parent private Key<ComicObject> comic;
	@Id private Long commentId;
    
	@Index private String commentorName;
    private String content;
    @Index private Date createdTime;

    public ComicCommentObject(){
        
    }

    public ComicCommentObject(String commentorName, String content, Long parentComicId){
        this.commentorName = commentorName;
        this.content = content;
        this.comic = Key.create(ComicObject.class, parentComicId);
        this.createdTime = new Date();
    }

    public Long getCommentId() {
    	return commentId;
    }
    
    public String getCommentorName() {
    	return commentorName;
    }
    public void setCommentorName(String commentorName) {
    	this.commentorName = commentorName;
    }
    
    public String getContent() {
    	return content;
    }
    public void setContent(String content) {
    	this.content = content;
    }

    public Date getCreatedTime() {
        return this.createdTime;
    }

}
