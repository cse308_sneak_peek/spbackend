package com.example.saveObj;

import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

@Entity
public class ComicObject implements Serializable {
	@Id private Long comicId; 
	
	@Index private String authorName;
	@Index private String comicName;
	@Index private String comicDescription;
	private String comicCover;
	@Index private Date createdTime;
    @Index private ArrayList<String> tags;
    @Index private String comicGenre;
    @Index private int numSubscribed;
    @Index private int numOfChapters;
    private String latestChaptername="";
    
    public ComicObject(){
    	
    }
    public ComicObject(String authorId, String comicName){
    	authorName=authorId;
    	this.comicName=comicName;
    	comicCover="";
    	comicDescription="";
    	createdTime=new Date();
    	comicGenre="";
    	numSubscribed=0;
    	tags=new ArrayList<String>();
    }
    public ComicObject(String authorId, String comicName, String comicDescription, String comicGenre){
    	authorName = authorId;
        this.comicName = comicName;
        this.comicDescription = comicDescription;
        this.createdTime = new Date();
        tags = new ArrayList<String>();
        this.comicGenre = comicGenre;
        this.numSubscribed = 0;
        numOfChapters=0;
    }
    public String getComicCover() {
		return comicCover;
	}
	public void setComicCover(String comicCover) {
		this.comicCover = comicCover;
	}
    public int getNumOfChapters(){
    	return numOfChapters;
    }
    public void setNumOfChapters(int numOfChapters){
    	this.numOfChapters=numOfChapters;
    }
    public void addNumOfChapters() {
    	this.numOfChapters++;
    }
    
    public Long getComicId() {
    	return comicId;
    }

    public String getComicName() {
    	return comicName;
    }
    public void setComicName(String comicName) {
    	this.comicName = comicName;
    }
    
    public String getAuthorName() {
    	return authorName;
    }
    public void setAuthorName(String authorId) {
    	authorName = authorId;
    }
    
    public String getComicDescription() {
    	return comicDescription;
    }
    public void setComicDescription(String comicDescription) {
    	this.comicDescription = comicDescription;
    }
    
    public Date getCreatedTime() {
    	return createdTime;
    }
    public void setCreatedTime(Date createdTime) {
    	this.createdTime = createdTime;
    }
    
    public ArrayList<String> getTags() {
    	return tags;
    }
    public void setTags(ArrayList<String> tags) {
    	this.tags = tags;
    }
    public void addTag(String tag) {
		tags.add(tag);
    }
    
    public String getComicGenre() {
    	return comicGenre;
    }
    public void setComicGenre(String comicGenre) {
    	this.comicGenre = comicGenre;
    }
    
    public int getNumSubscribed() {
    	return numSubscribed;
    }
    public void setNumSubscribed(int increase) {
    	numSubscribed = numSubscribed + increase;
    }

    public String getLatestChaptername() {
        return latestChaptername;
    }

    public void setLatestChaptername(String latestChaptername) {
        this.latestChaptername = latestChaptername;
    }
}
