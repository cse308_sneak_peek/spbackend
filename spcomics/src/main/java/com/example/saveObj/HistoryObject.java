package com.example.saveObj;

import java.util.ArrayList;
import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class HistoryObject {
	@Id private Long historynId;
	
	@Index private String userId;
	@Index private Long comicId;
	@Index private String comicName;
	@Index private int chapterNum;
	@Index private String chapterName;
	@Index private Date viewedDate;
	
    public HistoryObject(){
    	
    }

    public HistoryObject(String userId, Long comicId, String comicName, int chapterNum, String chapterName){
    	this.userId = userId;
    	this.comicId = comicId;
    	this.comicName = comicName;
    	this.chapterNum = chapterNum;
    	this.chapterName = chapterName;
    	this.viewedDate = new Date();
    }
    
    public String getUserId() {
    	return userId;
    }
    public void setUserId(String userId) {
    	this.userId = userId;
    }
    
    public Long getComicId() {
    	return comicId;
    }
    public void setComicId(Long comicId) {
    	this.comicId = comicId;
    }
    
    public String getComicName() {
    	return comicName;
    }
    public void setComicName(String comicName) {
    	this.comicName = comicName;
    }
    
    public int getChapterNum() {
    	return this.chapterNum;
    }
    public void setChapterNum(int chapterNum) {
    	this.chapterNum = chapterNum;
    }
    
    public String getChapterName() {
    	return this.chapterName;
    }
    public void setChapterName(String chapterName) {
    	this.chapterName = chapterName;
    }
    
    public Date getViewedDate() {
    	return this.viewedDate;
    }
    public void setViewedDate() {
    	this.viewedDate = new Date();
    }
}