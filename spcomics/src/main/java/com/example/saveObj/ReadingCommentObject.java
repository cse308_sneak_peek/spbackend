package com.example.saveObj;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Entity
public class ReadingCommentObject {
    @Parent private Key<ChapterObject> chapter;
	@Id private Long commentId;
    
	@Index private String commentorName;
    private String content;
    @Index private Date createdTime;

    public ReadingCommentObject(){
        
    }

    public ReadingCommentObject(String commentorName, String content, Key<ChapterObject> parentKey, Long chapterId){
        this.commentorName = commentorName;
        this.content = content;
        this.chapter = Key.create(parentKey, ChapterObject.class, chapterId);
        this.createdTime = new Date();
    }

    public Long getCommentId() {
    	return commentId;
    }
    
    public String getCommentorName() {
    	return commentorName;
    }
    public void setCommentorName(String commentorName) {
    	this.commentorName = commentorName;
    }
    
    public String getContent() {
    	return content;
    }
    public void setContent(String content) {
    	this.content = content;
    }

    public Date getCreatedTime() {
        return this.createdTime;
    }

}
