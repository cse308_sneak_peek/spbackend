package com.example.saveObj;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class SubscriptionObject {
	@Id private Long subscriptionId;
	
	@Index private String userId;
	@Index private Long comicId;
	@Index private Date createdTime;
	
    public SubscriptionObject(){
    	
    }

    public SubscriptionObject(String userId, Long comicId){
    	this.userId = userId;
    	this.comicId = comicId;
    	this.createdTime = new Date();
    }
    
    public String getUserId() {
    	return userId;
    }
    public void setUserId(String userId) {
    	this.userId = userId;
    }
    
    public Long getComicId() {
    	return comicId;
    }
    public void setComicId(Long comicId) {
    	this.comicId = comicId;
    }

    public Date getCreatedTime() {
    	return createdTime;
    }
    public void setCreatedTime(Date time) {
    	createdTime = time;
    }
}
