package com.example.saveObj;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Entity
public class UserDatastore implements Serializable {
    @Id private Long id;
    @Index private String userId;
    private String nickname;
    private String pic_uri;

    public UserDatastore() {
    }

    public UserDatastore(String userId) {
        this.userId = userId;
        setNickname("Anonymous");
    }

    public Long getId() {
    	return this.id;
    }
    
    public String getUserId() {
        return userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPic_uri() {
        return pic_uri;
    }

    public void setPic_uri(String pic_uri) {
        this.pic_uri = pic_uri;
    }
}
