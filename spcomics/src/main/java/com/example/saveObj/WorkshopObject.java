package com.example.saveObj;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class WorkshopObject {

    @Id Long id;

    private String userId;
    private String value;
    private String typeOf;
    private String name;
    private String secondaryValue;
    public WorkshopObject(){

    }

    public WorkshopObject(String id, String val, String secondaryValue, String typeOf, String name){
        userId=id;
        value=val;
        this.secondaryValue=secondaryValue;
        this.typeOf = typeOf;
        this.name=name;
    }
    
    public String getSecondaryValue(){
    	return secondaryValue;
    }
    public void setSecondaryValue(String secondaryValue){
    	this.secondaryValue=secondaryValue;
    }
    public String getName(){
    	return name;
    }
    
    public void setName(String name){
    	this.name=name;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getType(){
    	return typeOf;
    }
    public void setType(String type){
    	typeOf=type;
    }




}
