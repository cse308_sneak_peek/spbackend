/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.spcomics;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author yrao
 */
public class Comic {
    private int comicId;
    private int authorId;
    private String comicName;
    private Date createdTime;
    private String comicDescription;
    private List<String> comicChapters;
    private HashMap comments;
    private String genre;
    private List<String> tags;
    private double rating;
    private int numSubscribed;
    public Comic(int authorId, int comicId, String comicName, String comicDescription, String genre, List tags){
        this.authorId=authorId;
        this.comicId=comicId;
        this.comicName=comicName;
        this.comicDescription=comicDescription;
        this.genre=genre;
        this.tags=tags;
        createdTime=new Date();
        comicChapters=Collections.<String>emptyList();
        comments=new HashMap();
        rating=-1; // | -1 means no ratings yet |
        numSubscribed=0;
    }
    public int getComicId() {return comicId;}
    public void setComicId(int comicId) {this.comicId = comicId;}
    public int getAuthorId() {return authorId;}
    public void setAuthorId(int authorId) {this.authorId = authorId;}
    public String getComicName() {return comicName;}
    public void setComicName(String comicName) {this.comicName = comicName;}
    public Date getCreatedTime() {return createdTime;}
    public void setCreatedTime(Date createdTime) {this.createdTime = createdTime;}
    public String getComicDescription() {return comicDescription;}
    public void setComicDescription(String comicDescription) {this.comicDescription = comicDescription;}
    public List getComicChapters() {return comicChapters;}
    public void setComicChapters(List comicChapters) {this.comicChapters = comicChapters;}
    public HashMap getComments() {return comments;}
    public void setComments(HashMap comments) {this.comments = comments;}
    public String getGenre() {return genre;}
    public void setGenre(String genre) {this.genre = genre;}
    public List getTags() {return tags;}
    public void setTags(List tags) {this.tags = tags;}
    public double getRating() {return rating;}
    public void setRating(double rating) {this.rating = rating;}
    public int getNumSubscribed() {return numSubscribed;}
    public void setNumSubscribed(int numSubscribed) {this.numSubscribed = numSubscribed;}
}
