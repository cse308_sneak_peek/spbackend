/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.spcomics;

import java.util.Collections;
import java.util.List;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author yrao
 */
public class ComicChapter {
    private int chapterId;
    private int comicId;
    private String chapterName;
    private Date createdChapterTime;
    private HashMap comments;
    private List<Integer> ratings;
    private boolean isPublished;
    public ComicChapter(int chapterId, int comicId, String chapterName){
        this.chapterId=chapterId;
        this.chapterName=chapterName;
        this.comicId=comicId;
        createdChapterTime = new Date();
        comments = new HashMap();
        ratings = Collections.<Integer>emptyList();
        isPublished = false;
    }
    public int getChapterId() {return chapterId;}
    public void setChapterId(int chapterId) {this.chapterId = chapterId;}
    public int getComicId() {return comicId;}
    public void setComicId(int comicId) {this.comicId = comicId;}
    public String getChapterName() {return chapterName;}
    public void setChapterName(String chapterName) {this.chapterName = chapterName;}
    public Date getCreatedChapterTime() {return createdChapterTime;}
    public void setCreatedChapterTime(Date createdChapterTime) {this.createdChapterTime = createdChapterTime;}
    public HashMap getComments() {return comments;}
    public void setComments(HashMap comments) {this.comments = comments;}
    public List getRatings() {return ratings;}
    public void setRatings(List ratings) {this.ratings = ratings;}
    public boolean isIsPublished() {return isPublished;}
    public void setIsPublished(boolean isPublished) {this.isPublished = isPublished;}
}
