/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.spcomics;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author yrao
 */
public class User {
    private int userId;
    private HashMap history;
    private HashMap myComments; //My comments on other profile pages
    private HashMap otherComments; // Comments made on my profile page
    private List<Comic> myCreatedComics; // List of my own comics
    //private List<User> mySubscription; // Who is subscribed to me
    private List<Comic> mySubscribed;  // Which comics am I subscribed to
    private String lastName;
    private String firstName;
    public boolean profilePic;
    public User(int userId, String firstName, String lastName){
        this.firstName=firstName;
        this.lastName=lastName;
        this.userId=userId;
        myComments = new HashMap();
        otherComments = new HashMap();
        history = new HashMap();
        myCreatedComics = Collections.<Comic>emptyList();
        //mySubscription = new List();
        mySubscribed = Collections.<Comic>emptyList();
        profilePic = false;
    }
    public int getUserId() {return userId;}
    public void setUserId(int userId) {this.userId = userId;}
    public HashMap getHistory() {return history;}
    public void setHistory(HashMap history) {this.history = history;}
    public HashMap getMyComments() {return myComments;}
    public void setMyComments(HashMap myComments) {this.myComments = myComments;}
    public HashMap getOtherComments() {return otherComments;}
    public void setOtherComments(HashMap otherComments) {this.otherComments = otherComments;}
    public List getMyCreatedComics() {return myCreatedComics;}
    public void setMyCreatedComics(List myCreatedComics) {this.myCreatedComics = myCreatedComics;}
    //public List getMySubscription() {return mySubscription;}
    //public void setMySubscription(List mySubscription) {this.mySubscription = mySubscription;}
    public List getMySubscribed() {return mySubscribed;}
    public void setMySubscribed(List mySubscribed) {this.mySubscribed = mySubscribed;}
    public String getLastName() {return lastName;}
    public void setLastName(String lastName) {this.lastName = lastName;}
    public String getFirstName() {return firstName;}
    public void setFirstName(String firstName) {this.firstName = firstName;}
}
