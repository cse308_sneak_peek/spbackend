<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="javascripts/jquery.zlight.menu.1.0.min.js"></script> <!--reading nav js-->
    <script src="javascripts/respond.min.js"></script>
    <script src="javascripts/jquery.lazyload.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/sp.css">
    <script>
        $(document).ready(function(){
            $('#zlight-nav').zlightMenu();
        });
    </script>
    <script language="javascript">
        function aab(){
            aaa=""
            ja=""
            for(i=101,a=60;i<160;i++,a--){
                ja+="<div style=\"overflow:hidden;width:100%;height:1px;position:absolute;top:" + i + "px; left:0px; filter:alpha(opacity=" + a + "); opacity:" + (a*0.01) + "\"><img src=\"images/banner3.jpg\" style=\"margin:-" + (100-(i-100)) + "px 0px 0px 0px\"></div>"
            }
            banner.innerHTML+=ja
        }
    </script>
    <style>
        .card-title{
            font-size:14px !important;
            font-weight: 300;
        }
    </style>
</head>

<body  onload="aab()">
<div id="banner">
    <img class="bannerImg" src="images/banner3.jpg" width"100%" height="100">
</div>

<div class="ex_container">
    <div class="ex_nav mycard">
        <div class="col-lg-12" style="padding:0">
            <nav id="zlight-nav" class="mycard">
                <ul id="zlight-main-nav">
                    <li class="zlight-dropdown"><a href="/explore">EXPLORE </a></li>
                    <li class="zlight-active"><a href="#">AllComicSeries </a></li>

                </ul>
                <!-- MOBILE NAV -->
                <div id="zlight-mobile-nav">
                    <span>Menu</span>
                    <i class="icon-reorder zlight-icon"></i>
                    <select></select>
                </div>
            </nav> <!-- nav close -->
        </div>
    </div>
    <div class="exploreContent">
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                A
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty A}">
                <c:forEach items="${A}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Series Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>

        </div>

        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                B
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty B}">
                <c:forEach items="${B}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>

        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                C
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty C}">
                <c:forEach items="${C}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>

        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                D
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty D}">
                <c:forEach items="${D}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>

        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                E
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty E}">
                <c:forEach items="${E}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>


        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                F
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty F}">
                <c:forEach items="${F}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>


        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                G
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty G}">
                <c:forEach items="${G}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>


        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                H
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty H}">
                <c:forEach items="${H}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>


        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                I
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty I}">
                <c:forEach items="${I}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>



        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                J
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty J}">
                <c:forEach items="${J}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>



        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                K
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty K}">
                <c:forEach items="${K}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>



        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                L
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty L}">
                <c:forEach items="${L}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>



        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                M
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty M}">
                <c:forEach items="${M}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>


        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                N
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty N}">
                <c:forEach items="${N}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>



        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                O
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty O}">
                <c:forEach items="${O}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>


        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                P
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty P}">
                <c:forEach items="${P}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>




        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                Q
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty Q}">
                <c:forEach items="${Q}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>



        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                R
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty R}">
                <c:forEach items="${R}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>



        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                S
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty S}">
                <c:forEach items="${S}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a> <p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>


        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                T
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty T}">
                <c:forEach items="${T}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>



        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                U
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty U}">
                <c:forEach items="${U}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>


        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                V
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty V}">
                <c:forEach items="${V}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a> <p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>


        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                W
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty W}">
                <c:forEach items="${W}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>



        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                X
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty X}">
                <c:forEach items="${X}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>



        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                Y
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty Y}">
                <c:forEach items="${Y}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a> <p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>



        <hr>
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                Z
            </div>
        </div>

        <div class="row">
            <c:if test="${not empty Z}">
                <c:forEach items="${Z}" var="item">
                    <!--one item-->
                    <div class="col s6 m4 l2">
                        <div class="card  mycard">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="${item.comicCover}" alt="Cover Image">
                            </div>
                            <div class="card-content">
                                <a href="/comicdescription?comicId=${item.comicId}"><span class="card-title activator grey-text text-darken-4">${item.comicName}<i class="material-icons right">more_vert</i></span>
                                </a><p><a href='/authorpage?author=<c:out value="${item.authorName}"/>'>${item.authorName}</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
                                <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>

    </div>
</div>



<p class="copyright">© Spcomic.</p>
<script>
    $(function() {
        $("img.lazy").lazyload({
            effect : "fadeIn"
        });
    });
</script>

</body>
</html>