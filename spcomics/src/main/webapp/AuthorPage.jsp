<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" >
    <link type="text/css" rel="stylesheet" href="css/sp.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <script src="javascripts/jquery-2.2.2.js"></script>
    <script src="javascripts/sp.js"></script>
    <title>Profile Page</title>

</head>

<body>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="javascripts/bootstrap.min.js"></script>

<div class="profile_container">
    <div class="h-container card" style="background-image: url(images/profile_bg.png);">
        <div class="h-main">
            <div class="h-user-info unfollowed">
                <div class="h-avatar-container">
                    <div class="h-avatar">
                        <a href="#" ><img src="images/user_temp.png" id="h-avatar" come-on-click-me!!=""></a>
                    </div>
                </div>

                <div class="h-meta">
                    <div class="h-name">
                        <span id="h-name">${authorName}</span>
                    </div>

                </div>
            </div>

            <div class="h-bg-container overlay">
                <div class="h-bg-blur overlay"></div>
                <div class="h-bg overlay" style="background-image:url(images/profile_bg.png);"></div>
            </div>
        </div>
    </div>
    <br />

    <p>
        <span id="nickname_warning" style="display:none;">Your new nickname will be reflected next time when you log in. </span>
    </p>

    <div class="scrollable">
        <div class="scrollable_inner">

            <c:if test="${not empty requestScope.authorcomics}">
                <c:forEach items="${requestScope.authorcomics}" var="item">
                    <!--one item-->
                    <div class="displayItem mycard">
                        <div>

                            <a href="/comicdescription?comicId=${item.comicId}">
                                <img class="img" id="" data-key=""  src="${item.comicCover}" />

                            </a>
                        </div>
                        <div class="detail">
                            <div class="title">
                                <a href="/comicdescription?comicId=${item.comicId}">
                                        ${item.comicName}
                                </a>
                            </div>
                            <div class="author">
                                <a href='/authorpage?author=<c:out value="${item.authorName}"/>'>
                                        ${item.authorName}
                                </a>
                            </div>
                            <div class="description">
                                    ${item.comicDescription}
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>
    </div>
</div>

</div>
</body>
</html>
