<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
		 pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Add New Chapter</title>
</head>
<body>
<div id="imgTest" style="visibility:hidden"></div>
<div class="uploadChapter">
	<form action="/workshopChapter" method="post">
		<div class="row">
			<label>Chapter Name: </label>
			<input id="chapterName" name="chapterName" value="" size="32" maxlength="32" />
		</div>
		<div class="line_Space"></div>
		<div class="row">
			<input id="inputImage" type="file" accept="image/png" onchange="loadImage()"/>
			<label type="hidden"><font color="red">Please follow the format for multiple images: "CHAPTERNAME_CHAPTERNUMBER"</font></label>
			<input id="multinputImage" multiple="multiple" type="hidden" type="file" accept="image/png" onchange="multiloadImage()"/>
			<input type="hidden" id="fileInput" name="fileInput">
			<input type="hidden" id="comicId" name="comicId">
			<input type="hidden" id="chapNum" name="chapNum">
		</div>
		<div class="line_Space"></div>
		<input class="button" type="submit" value="save"/>
		<button class="button">Cancel</button>
	</form>
	<script type="text/javascript">
		function loadImage(){
			var filesSelected = document.getElementById("inputImage").files;
			inputFile = filesSelected[0];
			console.log(inputFile);
			var fileReader = new FileReader();
			fileReader.onload = function(fileLoadedEvent) {
				var srcData = fileLoadedEvent.target.result; // <--- data: base64
				var newImage = document.createElement('img');
				newImage.style.visibility="hidden";
				newImage.src = srcData;
				//lc.saveShape(LC.createShape('Image',{x:0,y:0,image:newImage}));
				var dataURL=document.getElementById("imgTest").innerHTML;
				var currentComicId = '${sessionScope.currentComic.comicId}';
				var currentComicNum = parseInt('${sessionScope.currentComic.numOfChapters}');
				var currentComicAuthor = '${sessionScope.currentComic.authorName}';
				currentComicNum = currentComicNum + 1;
				console.log(currentComicNum);
				//document.getElementById("chapNum").value=currentComicNum;
				document.getElementById("comicId").value="${sessionScope.currentComic.comicId}";
				document.getElementById("fileInput").value=srcData;
				document.getElementById("chapNum").value=currentComicNum;
				//document.getElementById("chapterNum").value=currentComicNum;
				document.getElementById("imgTest").innerHTML = newImage.outerHTML;
			}
			fileReader.readAsDataURL(inputFile);
		}
	</script>
</div>
</body>
</html>