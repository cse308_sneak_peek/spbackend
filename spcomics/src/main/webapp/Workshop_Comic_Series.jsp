<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<title>Workshop Comic Series</title>
	
	<!--Materialize-->
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<link type="text/css" rel="stylesheet" href="css/sp.css">
</head>

<body>
	<script type="text/javascript" src="js/materialize.min.js"></script>
<div id="imgTest" width="100" height="100" style="visibility:hidden"></div>
<div class="seriers_detail">
	<div class="info_detail">
		<div class="info_cover">
                <img id="cover_image" class="cover_image" height="100" width="100" src="${sessionScope.currentComic.comicCover}" alt="Cover Image"/>
            </div>
		<div class="info_about">
			<p>
				<span class="author">${sessionScope.user_info.userId}</span>
			</p>
			<span class="name">${csName}</span>

			<div class="about_bottom">
				<div class="file-field input-field">
					<div class="btn">
						<span>Choose File</span>
						<input id="inputImage" class="btn" type="file" accept="image/png" value="Load Local Image">
					</div>
					
					<div class="file-path-wrapper" style="float:left">
						<input class="file-path validate" type="text">
					</div>
					<div class="btn btn-primary waves-effect waves-light teal lighten-1" style="float:left; margin-left:10px">
						<button class="teal lighten-1" type="button" onclick="changeCover()" >Change Cover</button>
					</div>
				</div>
				<a href="#" class="waves-effect waves-light btn" onclick="toWorkshop()">New Chapter</a>
			</div>
		</div>
	</div>

	<!--tag-->
	<div class="info_tags" style="margin-left: -50px;">
		<div class="info_tag" style="padding:0px 0 10px 10px">Tags: </div>
		<c:set var="comicTags" value="${cs.tags}" />
		<c:choose>
			<c:when test="${not empty comicTags}">
				<ul style="display:inline-block">
					<c:forEach var="eachTag" items="${comicTags}">
						<li class="chip blue"><a href="/searching?keyword=${eachTag}"><span style="color:white">${eachTag}</span></a></li>
					</c:forEach>
				</ul>
			</c:when>
			
			<c:otherwise>
				<span>No Tags.</span>
			</c:otherwise>
		</c:choose>
	</div>

	<hr style="margin-top: 0px;"/>
	<div class="ws_text">
		<p>Description</p>
		<div class="ws_text_sub">
			${cs.comicDescription}
		</div>
	</div>

	<hr />
	<div class="mycard comic_list">
		<div class="comic_list_ul">
			<table class="highlight">
				<thead>
					<tr>
						<th>Publish</th>
						<th>Chapter Number</th>
						<th>Chapter Name</th>
						<th>Upload Time</th>
					</tr>
				</thead>
				
				<tbody>
					<c:if test="${not empty listofComicChapters}">
						<c:forEach  var="item" items="${listofComicChapters}">
							<tr>
								<c:choose>
									<c:when test="${item.isPublished=='true'}">
											<td><i class="material-icons">done</i></td>
											<td>${item.chapterNum}</td>
											<td>
												<a href='/readingComments?chapterName=<c:out value="${item.chapterName}"/>'>
												${item.chapterName}
												</a>
											</td>
											<td>${item.createdTime}</td>
		
									</c:when>
									<c:otherwise>
											<td><button onclick="publishChapter(${item.chapterNum})" type="button" value="Not Published" style="background-color:white;"><i class="material-icons">input</i></button></td>
											<td>${item.chapterNum}</td>
											<td>
												<a href='/workshop?chapterNumber=<c:out value="${item.chapterNum}"/>'>
													${item.chapterName}
												</a>
											</td>
											<td>${item.createdTime}</td>
									</c:otherwise>
								</c:choose>
							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
	</div>
	
	<script type="text/javascript">
	
	function changeCover(){
		//var currentComic = '${sessionScope.currentComic.comicCover}';
		var img = document.getElementById("cover_image");
		var filesSelected = document.getElementById("inputImage").files;
		inputFile = filesSelected[0];
		console.log(inputFile);
		var fileReader = new FileReader();
        fileReader.onload = function(fileLoadedEvent) {
            var srcData = fileLoadedEvent.target.result; // <--- data: base64
            var newImage = document.createElement('img');
            newImage.style.visibility="hidden";
            newImage.src = srcData;
            //lc.saveShape(LC.createShape('Image',{x:0,y:0,image:newImage}));
        	var dataURL=document.getElementById("imgTest").innerHTML;
            document.getElementById("imgTest").innerHTML = newImage.outerHTML;
           // alert("Converted Base64 version is "+JSON.stringify(document.getElementById("imgTest").innerHTML).substring(34));
           // console.log("Converted Base64 version is "+document.getElementById("imgTest").innerHTML);
        	document.getElementById("coverSrc").value=srcData;
        	document.getElementById("comicCover").submit();
            
        	//document.getElementById("coverSrc").value=JSON.stringify(document.getElementById("imgTest").innerHTML).split('\"');
        	//lc.saveShape(LC.createShape('Image',{x:10,y:10,image:newImage}));
            //lc.loadSnapshot(JSON.parse(JSON.stringify((document.getElementById("imgTest").innerHTML).substring(indexOf(",")))));	
        }
        fileReader.readAsDataURL(inputFile);
		/*
		var width=img.clientWidth;
		var height=img.clientHeight;
		if(width!=200 && height!=303){
			alert("Please change your cover image to 300x455.");
		}
		console.log("Width="+width+";Height="+height);
		*/
		
	}
	
	function uploadPages(){
		var filesSelected = document.getElementById("nc").files;
		inputFile = filesSelected[0];
		console.log(inputFile);
		var fileReader = new FileReader();
		fileReader.onload = function(fileLoadedEvent) {
			var srcData = fileLoadedEvent.target.result;
			var newImage = document.createElement('img');
			newImage.style.visibility="hidden";
			newImage.src = srcData;
		}
		fileReader.readAsDataURL(inputFile);
	}
	function toWorkshop(){
		var comicChapterNum = ${sessionScope.currentComic.numOfChapters};
		var currentComic = ${sessionScope.currentComic.comicId};
		console.log(comicChapterNum);
		//if(comicChapterNum===0){
			var chapter_name = prompt("Please enter a name for your new chapter: ");
			//var chapter_num = 1;
			if(chapter_name===null){
				return;
			}
			else{
				document.getElementById("comicId").value=currentComic;
				document.getElementById("chapter_num").value=comicChapterNum+1;
				document.getElementById("chapter_name").value=chapter_name;
				document.getElementById("wsChapterForm").submit();
			}
			//console.log("ZERO CHAPTERS YOU FOOL");
			//var href='/workshop';
			//window.location=href;
		//}
		//else{
		//}
	}
	
	function publishChapter(num){
		console.log("Chapter number is " + num);
		var r=confirm("Are you sure you want to publish?");
		if(r==true){
			document.getElementById("publish_num").value=num;
			document.getElementById("publishForm").submit();
		}
	}
</script>
<form id="comicCover" action="/comicCover" method="post">
	<input type="hidden" id="coverSrc" name="coverSrc"/>
</form>
<form id="chapterForm" action="/workshopChapter" method="post">
	<input type="hidden" id="chapterName" name="chapterName"/>
	<input type="hidden" id="chapterNumber" name="chapterNumber"/>
	<input type="hidden" id="chapterImages" name="chapterImages"/>
	<input type="hidden" id="comicName" name="comicName"/>
</form>
<form id="wsChapterForm" action="/newChapter" method="post">
	<input type="hidden" id="chapter_name" name="chapter_name"/>
	<input type="hidden" id="chapter_num" name="chapter_num"/>
	<input type="hidden" id="comicId" name="comicId"/>
</form>
<form id="publishForm" action="/publishChapter" method="post">
	<input type="hidden" id="publish_num" name="publish_num"/>
</form>
</body>
</html>