<%@ page import="java.util.List" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!--Import jQuery before materialize.js-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    
    <!--reading nav js-->
    <script src="javascripts/jquery.zlight.menu.1.0.min.js"></script>
    <script src="javascripts/respond.min.js"></script>
    
    <!-- mdb -->
    <!-- Material Design Icons -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="css/mdb.css" rel="stylesheet">
    
    <link type="text/css" rel="stylesheet" href="css/sp.css">
    
    <!--add overflow back to body, hide nav bar-->
    <script>
		$(document).ready(function(){
			var userId = "${sessionScope.user_info.userId}";
			var numOfChapters = "${comicInfo.numOfChapters}";
			if(userId == "") {
				$("#subscribeBT").hide();
			}
			if(numOfChapters == 0) {
				$("#readingBT").hide();
			}
			$("#zlight-nav").zlightMenu();
			$("#HCloseBtn").click(function(e){
				e.preventDefault();
				$("#rm_nav").css("display","none")	
			});
		});
	</script>
    
    
    <script>
    	var comicIdVal = "${comicInfo.comicId}";
    	$(document).ready(function() {
    		$("#subscribeBT").click(function(event) {
    			var url = "/subscription"; // the script where you handle the form input.
    			$.ajax({
    				   type: "POST",
    				   url: url,
    				   data: {comicId: comicIdVal, Subscribe: $(this).text()},
    				   success: function() {
    					   
    					   if($("#subscribeBT").text() === "Subscribe") {
    						   $("#subscribeBT").text("Unsubscribe");
    						   $("#subscribeNum").text(parseInt($("#subscribeNum").text()) + 1);
    					   } else {
    						   $("#subscribeBT").text("Subscribe");
    						   $("#subscribeNum").text(parseInt($("#subscribeNum").text()) - 1);
    					   }
    				   },
    				   error: function() {
							
    				   }
    			});
    			return false; // avoid to execute the actual submit of the form.
    		});
    	});
		
	</script>

    <title>Commic Page</title>
</head>

<body>
	<!--reading mode nav-->
    <div id="rm_nav" style="display:none">
		<div class="col-lg-12" style="padding: 0">
			<nav id="zlight-nav" class="mycard">
				<ul id="zlight-main-nav">
					<li class="zlight-dropdown moveChapter LastChapter"><a
						href="#" title="Last Charpter"> <i class="material-icons"
							style="vertical-align: middle; font-size: 50px; transform: scale(-1, 1);">trending_flat</i></a></li>
					<li class="zlight-dropdown"><a href="#">${comicInfo.comicName}</a></li>
					<li><a href="#" id="HCloseBtn"><i class="material-icons"
							style="vertical-align: middle; font-size: 30px; line-height: 50px;">launch</i></a></li>
					<li class="zlight-dropdown moveChapter NextChapter"
						style="right: 0px; position: absolute;"><a href="#"
						title="NextCharpter"> <i class="material-icons"
							style="vertical-align: middle; font-size: 50px;">trending_flat</i></a></li>
				</ul>
				<!-- MOBILE NAV -->
				<div id="zlight-mobile-nav">
					<span>Menu</span> <i class="icon-reorder zlight-icon"></i> <select></select>
				</div>
			</nav>
			<!-- nav close -->
		</div>
		<div id="readingPages">
			<ul id="chapterPages">
				<c:if test="${not empty currentChapterPages}">
					<c:forEach items="${currentChapterPages}" var="item" varStatus="status">
						<li><img src="${item}" /></li>
					</c:forEach>
				</c:if>
			</ul>
		</div>
	</div>
	
    <c:set var="comicInfo" value="${requestScope.comicInfo}" />
    <c:set var="chapterList" value="${requestScope.chapterList}" />
    <c:set var="commentList" value="${requestScope.commentList}" />
    <c:set var="isSubscribed" value="${requestScope.isSubscribed }" />
    
    <div class="mycontainer">
        <div class="banner"></div>
        <!--left section-->
        <div class="col-lg-8 col-md-8">
            <div class="mycard comic_info">
                <div class="info_detail border_bottom">
                    <div class="info_cover">
                        <img class="cover_image" src="${comicInfo.comicCover}" alt="Cover Image" aria-hidden="true">

                    </div>
                    <div class="info_about">
                        <p class="author">${comicInfo.authorName}</p>
                        <p class="name">${comicInfo.comicName}</p>
                         
                        <div class="about_bottom">
                            <c:set var="buttonValue" value="Subscribe" />
                            <c:if test="${isSubscribed == true }">
                                <c:set var="buttonValue" value="Unsubscribe" />
                            </c:if>
                            <div>
                            	<input type="hidden" id="currentComicId" value="${comicInfo.comicId}" />
								<a href="/readingComments?comicId=${comicInfo.comicId}&chapterNum=1">
									<button id="readingBT" class="readingForm btn btn-primary waves-effect waves-light" value="1">Start Reading</button>
								</a>
                                <button id="subscribeBT" type="submit" class="btn btn-primary waves-effect waves-light" value="${buttonValue}">${buttonValue}</button>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="info_right">
                        <div class="rating_container"> 
                            <a class="info-star-rating id-no-nav" href="#details-reviews">  
                                <div class="tiny-star star-rating-non-editable-container" aria-label=" Rated 4.2 stars out of five stars "> 
                                    <div class="current-rating" jsname="" style="width: 85%;"></div> 
                                </div>  
                            </a> 
                            <span class="ratings-detail"> 
                                <span class="rating-count" aria-label=" 16 ratings ">16</span> 
                                <span class="reviewers-img"></span> 
                            </span> 
                        </div>
                        
                        <div class="sub_container">
                            <p><label>Subscribed: </label></p>
                            <label id="subscribeNum" name="">${comicInfo.numSubscribed}</label>
                        </div>
                    </div>
                </div>
                
                <!--tags -->
                <div class="info_tags border_bottom">
                    <div class="info_tag">Tags: </div>
                    <c:set var="comicTags" value="${comicInfo.tags}" />
                    <c:choose>
                        <c:when test="${not empty comicTags}">
                            <c:forEach var="eachTag" items="${comicTags}">
                                <ul class="chip">
                                    <li><a href="/searching?byTag=true&keyword=${eachTag}">${eachTag}</a></li>
                                </ul>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                        	<span>No Tags.</span>
                        </c:otherwise>
                    </c:choose>
                </div>
                
                <!--descriptions -->
                <div class="info_description">
                    <div class="text">
                    <p><b>Comic Description</b></p>
                        ${comicInfo.comicDescription}
                    </div>
                </div>
            </div>
            
            <div class="mycard comic_list" margin-top:50px;>
                <ul class="comic_list_ul">
                    <li>
                        <span class="tx">Ch.</span>
                        <span class="subj"><span>Chapter Name</span></span>
                        <span class="date">Upload Time</span>	
                    </li>
                    <c:choose>
                        <c:when test="${not empty chapterList}">
                            <c:forEach var="chapter" items="${chapterList}">
                                <c:set var="createdTime" value="${chapter.createdTime}" />
                                <fmt:formatDate type="date" var="formattedCreatedTime" value="${createdTime}" />							
                                    <li class="readingForm" value="${chapter.chapterNum}" id="${chapter.chapterName}">
                                   		<a href="/readingComments?comicId=${comicInfo.comicId}&chapterNum=${chapter.chapterNum}">
                                            <span class="tx">#${chapter.chapterNum}</span>
                                            <span class="subj">${chapter.chapterName}</span>
                                            <span class="date">${formattedCreatedTime}</span>
                                        </a>
                                    </li>
                                
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <span>No chapters yet.</span>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>
        </div>
        
        <!--right section-->
        <div class="col-md-4 col-lg-4">
            <div class="mycard">
                <div class="cm_cell">
                    Post a new comment
                </div>
                
                <!--new comment-->
                <div class="cm_cell">
                    <form method="post"  action="/comicdescription">
                        <input type="hidden" id="comicId" name="comicId" value="${comicInfo.comicId}" />
                        <textarea name="content" maxlength="10000" class="mll" id="reply_content" style="overflow: hidden; word-wrap: break-word; resize: none; height: 112px;"></textarea>
                        <div class="sep10"></div>
                        <div class="fr">
                            <div class="sep5"></div>
                            <span class="gray"></span>
                        </div>
                        <input type="hidden" value="41464" name="once">
                        <input id="submitCommnetBT" type="submit" value="Comment" class="btn btn-primary waves-effect waves-light">
                        <script type="text/javascript">
                            $("#reply_content").keydown(function(e) {
                                if ((e.ctrlKey || e.metaKey) && e.which === 13) {
                                    e.preventDefault();
                                    $("#reply_content").parent().submit();
                                }
                            });
                        </script>
                    </form>
                </div>
                
                <!--user comment list-->
                <div class="commentbox">
                    <!-- this is one comment-->
    
                    <c:if test="${not empty commentList}">
                        <c:set var= "newline" value="<br />" />
                        <c:forEach var="item" items="${commentList}">
                        	<c:set var="commentTime" value="${item.createdTime}" />
                        	<fmt:formatDate type="both" var="formattedCommentTime" value="${commentTime}" />
                            <div id="r_3057080" class="cm_cell">
                                <table cellpadding="0" cellspacing="0"  width="100%">
                                    <tbody>
                                    <tr>
                                        <td width="48" valign="top" align="center"><img src="images/profile_pic.jpg" class="cm_avatar"  align="default"></td>
                                        <td width="10" valign="top"></td>
                                        <td width="auto" valign="top" align="left">
                                            <div class="sep3"></div>
                                            <strong><a href="/member/Hodor"> ${item.commentorName} </a></strong>
                                            &nbsp; &nbsp;
                                            <span class="small">${formattedCommentTime}</span>
                                            <div class="sep5"></div>
                                            <div class="reply_content"> ${item.content}</div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </c:forEach>
    
                    </c:if>
    
                </div>
            </div>
             
            <div class="suggest_list">
            </div>
        </div>
    </div>

    <div id="HBox" style="overflow:scroll;">

    </div>
	
    <script src="javascripts/jquery.hDialog.min.js"></script>
    <!--create reading mode window base on window size-->
     <script>
		$(function(){
			var $el = $('.dialog');
			$el.hDialog(); 
			$('.goDown').hDialog({height:$(window).height(), width:$(window).width(), effect: 'slideOutDown'});
		});
	</script>
</body>
</html>