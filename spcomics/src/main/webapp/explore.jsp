<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="javascripts/jquery.zlight.menu.1.0.min.js"></script> <!--reading nav js-->
    <script src="javascripts/respond.min.js"></script>
    <script src="javascripts/jquery.lazyload.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/sp.css">
    <script>
        $(document).ready(function(){
            $('#zlight-nav').zlightMenu();
        });
    </script>
    <script language="javascript">
        function aab(){
            aaa=""
            ja=""
            for(i=101,a=60;i<160;i++,a--){
                ja+="<div style=\"overflow:hidden;width:100%;height:1px;position:absolute;top:" + i + "px; left:0px; filter:alpha(opacity=" + a + "); opacity:" + (a*0.01) + "\"><img src=\"images/banner3.jpg\" style=\"margin:-" + (100-(i-100)) + "px 0px 0px 0px\"></div>"
            }
            banner.innerHTML+=ja
        }
    </script>
</head>

<body onload="aab()">
<div id="banner">
    <img class="bannerImg" src="images/banner3.jpg" width"100%" height="100">
</div>
<div class="ex_container">
    <div class="ex_nav mycard">
        <div class="col-lg-12" style="padding:0">
            <nav id="zlight-nav" class="mycard">
                <ul id="zlight-main-nav">
                    <li class="zlight-active"><a href="#">EXPLORE </a></li>
                    <li class="zlight-dropdown"><a href="/allcomics">AllComicSeries </a></li>

                </ul>
                <!-- MOBILE NAV -->
                <div id="zlight-mobile-nav">
                    <span>Menu</span>
                    <i class="icon-reorder zlight-icon"></i>
                    <select></select>
                </div>
            </nav> <!-- nav close -->
        </div>
    </div>
    <!--
    <div class="navbg">
        <ul id="navul">
            <li class=""><a href="#">EXPLORE</a></li>
            <li class=""><a href="#">POPULAR</a></li>
            <li class=""><a href="#">NEW RELEASES</a></li>
            <li class="">
                <a href="#">GENRES & MOODS</a>

                <ul>
                    <li><a href="#">aaaa</a></li>
                </ul>

            </li>
            <li class=""><a href="#">AUTHORS</a></li>
        </ul>
    </div>
     -->
    <div class="exploreContent">
        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                Recommended for You
            </div>
            <div class="scrollable">
                <div class="scrollable_inner">
                    <c:if test="${not empty recommandBySubscribed}">
                        <c:forEach items="${recommandBySubscribed}" var="item">
                            <!--one item-->
                            <div class="displayItem mycard">
                                <div class="cover_img">
                                    <a href="/comicdescription?comicId=${item.comicId}">
                                        <img class="img" id="" data-key="" src="${item.comicCover}" alt="Cover Image"/>
                                    </a>
                                </div>
                                <div class="detail">
                                    <div class="title">
                                        <a href="/comicdescription?comicId=${item.comicId}">
                                                ${item.comicName}
                                        </a>
                                    </div>
                                    <div class="author">
                                        <a href='/authorpage?author=<c:out value="${item.authorName}"/>'>
                                                ${item.authorName}
                                        </a>
                                    </div>
                                    <div class="description">
                                            ${item.comicDescription}
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </c:if>

                </div>
            </div>
        </div>

        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                Newly Updated Chapter
            </div>
            <div class="scrollable">
                <div class="scrollable_inner">
                    <c:if test="${not empty latestChapter}">
                        <c:forEach items="${latestChapter}" var="item">
                            <!--one item-->
                            <div class="displayItem mycard">
                                <div class="cover_img">
                                    <a href='/comicdescription?comicName=<c:out value="${item.chapterName}"/>'>
                                        <img class="img" id="" data-key=""  src="${item.comicCover}"  alt="Cover Image"/>
                                    </a>
                                </div>
                                <div class="detail">
                                    <div class="title">
                                        <a href="">
                                                ch:${item.chapterNum} : ${item.chapterName}
                                        </a>
                                    </div>
                                    <div class="series_name">
                                        <a href="">
                                               ${item.seriesName}
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </c:forEach>
                    </c:if>

                </div>
            </div>
        </div>


        <div class="exploreSubDiv">
            <div class="ex_feature_label">
                Newly Updated Series
            </div>
            <div class="scrollable">
                <div class="scrollable_inner">
                    <c:if test="${not empty latestSeries}">
                        <c:forEach items="${latestSeries}" var="item">
                            <!--one item-->
                            <div class="displayItem mycard">
                                <div class="cover_img">
                                    <a href='/comicdescription?comicName=<c:out value="${item.comicName}"/>'>
                                        <img class="img" id="" data-key=""  src="${item.comicCover}"  alt="Cover Image"/>
                                    </a>
                                </div>
                                <div class="detail">
                                    <div class="title">
                                        <a href='/comicdescription?comicName=<c:out value="${item.comicName}"/>'>
                                                ${item.comicName}
                                        </a>
                                    </div>
                                    <div class="author">
                                        <a href='/authorpage?author=<c:out value="${item.authorName}"/>'>
                                                ${item.authorName}
                                        </a>
                                    </div>
                                    <div class="description">
                                            ${item.comicDescription}
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </c:if>

                </div>
            </div>
        </div>


        <div>
            <div class="klbar">

            </div>
        </div>


    </div>

</div>
<p class="copyright">© Spcomic.</p>
<script>
    $(function() {
        $("img.lazy").lazyload({
            effect : "fadeIn"
        });
    });
</script>
<script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>