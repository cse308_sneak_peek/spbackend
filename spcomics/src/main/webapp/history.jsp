<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link href="css/his.css" rel="stylesheet" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="javascripts/jquery.mixitup.min.js"></script>
    <title>History </title>
</head>
<body>
	<c:set var="historyList" value="${historyList}" />
	<c:set var="viewedDateList" value="${viewedDateList}" />
	
    <div class="his_container">
        <div class="wrapper">
            <div class="main">
                <h1 class="title">History</h1>
                
				<c:choose>
					<c:when test="${not empty historyList}">
						<c:set var="index" value="0" />
						<c:forEach var="viewedDate" items="${viewedDateList}">
							<fmt:formatDate var="year" value="${viewedDate}" pattern="yyyy" />
							<fmt:formatDate var="month" value="${viewedDate}" pattern="MM" />
							<fmt:formatDate var="date" value="${viewedDate}" pattern="dd" />
							<div class="year">
			                    <h2><a href="#">${year}<i></i></a></h2>
			                    <div class="list">
			                        <ul>
			                            <li class="cls out_li">
			                                <p class="date">${month}/${date}</p>
			                                <div class="more card">
			                                    <ul>
			                                    	<c:forEach var="historyInOneDate" items="${historyList}" begin="${index}" end="${index}">
			                                    		<c:forEach var="history" items="${historyInOneDate}">
					                                        <li class="incard">
					                                            <a href="/comicdescription?comicId=${history.comicId}">
					                                                <div class="his_item">
					                                                    <div class="item_cover" style="background-image:url(images/Z.jpg);"></div>
					                                                    
					                                                    <div class="item_text">
					                                                        ${history.comicName}: #${history.chapterNum } ${history.chapterName}
					                                                    </div>
					                                                </div>
					                                            </a>
					                                        </li>
			                                        	</c:forEach>
			                                        </c:forEach>
			                                    </ul>
			                                </div>
			                            </li>
									</ul>
			                    </div>
			                </div>
			                <c:set var="index" value="${index+1}" />
		                </c:forEach>
					</c:when>
					<c:otherwise>
	               		<div>
	               			<p>No history yet.</p>
	               		</div>
	               	</c:otherwise>
				</c:choose>
			    
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>
        $(".main .year .list").each(function (e, target) {
            var $target=  $(target),
                $ul = $target.find("ul");
            $target.height($ul.outerHeight()), $ul.css("position", "inherit");
        }); 
        $(".main .year>h2>a").click(function (e) {
            e.preventDefault();
            $(this).parents(".year").toggleClass("close");
        });
    </script>
</body>
</html>