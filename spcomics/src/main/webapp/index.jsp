
<!doctype html>
<html>
<head>
    <title>Literally Canvas</title>
    <link rel='shortcut icon' href='favicon.ico' type='image/x-icon'>
    <link href="../_assets/literallycanvas.css" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <meta name="google-signin-client_id" content = "814863474691-b1mdcvo4bf3h116vgcg3jpj3peelhmbs.apps.googleusercontent.com">
    <style type="text/css">
        .fs-container {
            width: 720px;
            margin: auto;
        }

        .literally {
            width: 100%;
            height: 100%;
            position: relative;
        }
    </style>
</head>

<body>

<script>
    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        var userId=(profile.getEmail()).split("@")[0];

        window.location.href="/sign?id="+userId;
    }

    function onLoad() {
        gapi.load('auth2,signin2', function() {
            var auth2 = gapi.auth2.init();
            auth2.then(function() {
                // Current values
                var isSignedIn = auth2.isSignedIn.get();
                var currentUser = auth2.currentUser.get();

                if (!isSignedIn) {
                    // Rendering g-signin2 button.
                    gapi.signin2.render('google-signin-button', {
                        'onsuccess': 'onSignIn'
                    });
                }else{
                    window.location.href="/sign?";
                }
            });
        });

    }
</script>

<div id="google-signin-button"></div>

<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
</body>



</html>
