// JavaScript Document
(function($){
		$.fn.capacityFixed = function(options) {
			var opts = $.extend({},$.fn.capacityFixed.deflunt,options);
			var FixedFun = function(element) {
				var top = opts.top;
				element.css({
					"top":0
				});
				$(window).scroll(function() {
					var scrolls = $(this).scrollTop();
					if (scrolls > top) {
						if (window.XMLHttpRequest) {
							element.css({
								position: "fixed",
								background:"white",
								top: 0							
							});
						} else {
							element.css({
								top: scrolls
							});
						}
					}else {
						element.css({
							position: "absolute",
							top: 0
						});
					}
				});
			};
			return $(this).each(function() {
				FixedFun($(this));
			});
		};
		$.fn.capacityFixed.deflunt={
			top:110
		};
		})(jQuery);
		
