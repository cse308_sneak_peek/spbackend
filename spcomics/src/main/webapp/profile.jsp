<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" rel="stylesheet" href="css/sp.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <script src="javascripts/jquery-2.2.2.js"></script>
    <script src="javascripts/sp.js"></script>
    <title>Profile Page</title>
    <script>
        $(document).ready(function (){
            var input="${requestScope.warning_nickname}";
            if(input!=""){
                $("#nickname_warning").css('display','inline-block');
            }
        });
    </script>
</head>

<body>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

      <script type="text/javascript" src="js/materialize.min.js"></script>

<div class="profile_container">
    <div class="h-container card" style="background-image: url(images/profile_bg.png);">
        <div class="h-main">
            <div class="h-user-info unfollowed">
                <div class="h-avatar-container">
                    <div class="h-avatar">
                        <a href="#" ><img src="images/user_temp.png" id="h-avatar" come-on-click-me!!=""></a>
                    </div>
                </div>

                <div class="h-meta">
                    <div class="h-name">
                        <span id="h-name">${user_info.nickname}</span>
                    </div>
					<script>
						$(document).ready(function(){  
							var tds = $("#h-name");  
							tds.click(changeName);  
						});  
						   
						function changeName(){  
							var td = $(this);  
							var text = td.text();  
							td.html("");  
							var input = $("<input>");  
						  
							input.attr("value", text);  
							input.change(function(event) {  
								var cfm = confirm("Are you should changing to this name?");  
        						if(cfm){  
									var inputtext = input.val();  
									var tdNode = input.parent();  
									tdNode.html(inputtext);  
									tdNode.click(changeName);  
								}
								else{
									var tdNode = input.parent();  
									td.html(text)
									tdNode.click(changeName);
								}
							});  
							input.keyup(function(event) {  
								var myEvent = event || window.event;  
								var kcode = myEvent.keyCode;  
								if (kcode == 13) {  
									var cfm = confirm("Are you should changing to this name?");  
									if(cfm){
										var inputnode = $(this);  
										var inputtext = inputnode.val();  
										var tdNode = inputnode.parent(); 
										tdNode.html(inputtext);  
										tdNode.click(changeName);  
									}
									else{
										var tdNode = input.parent();  
										td.html(text)
										tdNode.click(changeName);
									}
								}  
							});  
							td.append(input);
							var inputdom = input.get(0);  
							inputdom.select();   
							td.unbind("click");  
						}
					</script>
                    <div class="h-sign empty-hint"> some profile info</div>
                </div>
            </div>

            <div class="h-bg-container overlay">
                <div class="h-bg-blur overlay"></div>
                <div class="h-bg overlay" style="background-image:url(images/profile_bg.png);"></div>
            </div>
        </div>
    </div>
    <br />
	
    <div class="mycard input-field">
        <form action="/sign" method="post">
			<div class="row">
				<div class="input-field col s6">
					<input value="${user_info.userId}" id="first_name2" name="change_nick" type="text" class="validate">
					<label class="active" for="first_name2">Alias</label>
				</div>
				<div class="input-field col s6" style="display:inline-block; ">
					<button class="waves-effect waves-light btn " type="submit">Change Alias</button>
				</div>
		  	</div>
			
			
        </form>
    </div>
	<script>
		$(document).ready(function() {
			Materialize.updateTextFields();
		  });
	</script>

    <div class="scrollable">
        <div class="scrollable_inner">

            <c:if test="${not empty sessionScope.authorcomics1}">
                <c:forEach items="${sessionScope.authorcomics1}" var="item">
                    <!--one item-->
                    <div class="displayItem mycard">
                        <div>
                            <a href="/comicdescription?comicId=${item.comicId}">
                                <img class="img" id="" data-key=""  src="images/Z-4.jpg" />
                            </a>
                        </div>
                        <div class="detail">
                            <div class="title">
                                <a href="/comicdescription?comicId=${item.comicId}">
                                        ${item.comicName}
                                </a>
                            </div>
                            <div class="author">
                                <a href='/authorpage?author=<c:out value="${item.authorName}"/>'>
                                        ${item.authorName}
                                </a>
                            </div>
                            <div class="description">
                                    ${item.comicDescription}
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>
    </div>
            </div>
    
</div>
</body>
</html>
