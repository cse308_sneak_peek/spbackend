<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Reading Page</title>
</head>
<style>
	ul {
		list-style-type: none;
	}
</style>
<body>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<div>
		<button onclick="previousChapter()">Previous Chapter</button>
		<button onclick="nextChapter()">Next Chapter</button>
		<script type="text/javascript">
			function previousChapter(){
				var chapterNum = '${sessionScope.chapterNum}';
				var numberChapters = '${sessionScope.currentComic.numOfChapters}';
				if(numberChapters>1 && chapterNum >1){
					var previousChapter = parseInt(chapterNum) - 1;
					console.log(chapterNum+"....Attempting to go to page "+previousChapter);
					window.location.href="/readingComments?chapterNum=" + previousChapter;
				}
			}
			function nextChapter(){
				var chapterNum = '${sessionScope.chapterNum}';
				var numberChapters = '${sessionScope.currentComic.numOfChapters}';
				if(chapterNum < numberChapters){
					var nextChapter = parseInt(chapterNum) + 1;
					console.log(chapterNum+"....Attempting to go to page "+nextChapter);
					window.location.href="/readingComments?chapterNum=" + nextChapter;
				}
			}
		</script>
	</div>
	<div id="readingPages">
	<ul id="chapterPages">
				<c:if test="${not empty currentChapterPages}">
					<c:forEach items="${currentChapterPages}" var="item" varStatus="status">
						<li> 
							<img src="${item}" /> 
						</li>
					</c:forEach>
				</c:if>
			</ul>
	</div>
	<div class="col-md-4 col-lg-4">
            <div class="mycard">
                <div class="cm_cell">
                    Post a new comment
                </div>
                
                <!--new comment-->
                <div class="cm_cell">
                    <form method="post"  action="/readingComments">
                    	<input type="hidden" id="comicId" name="comicId" value="${comicId}" />
                    	<input type="hidden" id="chapterNum" name="chapterNum" value="${chapterNum}" />
                        <input type="hidden" id="chapterId" name="chapterId" value="${chapterInfo.chapterId}" />
                        <textarea name="content" maxlength="10000" class="mll" id="reply_content" style="overflow: hidden; word-wrap: break-word; resize: none; height: 112px;"></textarea>
                        <div class="sep10"></div>
                        <div class="fr">
                            <div class="sep5"></div>
                            <span class="gray"></span>
                        </div>
                        <input type="hidden" value="41464" name="once">
                        <input id="submitCommnetBT" type="submit" value="Comment" class="btn btn-primary waves-effect waves-light">
                        <script type="text/javascript">
                            $("#reply_content").keydown(function(e) {
                                if ((e.ctrlKey || e.metaKey) && e.which === 13) {
                                    e.preventDefault();
                                    $("#reply_content").parent().submit();
                                }
                            });
                        </script>
                    </form>
                </div>
                
                <!--user comment list-->
                <div class="commentbox">
                    <!-- this is one comment-->
    
                    <c:if test="${not empty commentList}">
                        <c:set var= "newline" value="<br />" />
                        <c:forEach var="item" items="${commentList}">
                        	<c:set var="commentTime" value="${item.createdTime}" />
                        	<fmt:formatDate type="both" var="formattedCommentTime" value="${commentTime}" />
                            <div id="r_3057080" class="cm_cell">
                                <table cellpadding="0" cellspacing="0"  width="100%">
                                    <tbody>
                                    <tr>
                                        <td width="48" valign="top" align="center"><img src="images/profile_pic.jpg" class="cm_avatar"  align="default"></td>
                                        <td width="10" valign="top"></td>
                                        <td width="auto" valign="top" align="left">
                                            <div class="sep3"></div>
                                            <strong><a href="/member/Hodor"> ${item.commentorName} </a></strong>
                                            &nbsp; &nbsp;
                                            <span class="small">${formattedCommentTime}</span>
                                            <div class="sep5"></div>
                                            <div class="reply_content"> ${item.content}</div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </c:forEach>
    
                    </c:if>
    
                </div>
            </div>
             
            <div class="suggest_list">
            </div>
        </div>
</body>
</html>