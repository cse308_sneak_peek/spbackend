<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>search</title>
    <link type="text/css" rel="stylesheet" href="css/sp.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>
		$(document).ready(function(){
			$(".resultli").click(function(event) {
				
			});
		});
	</script>
</head>

<body class="searchpage">
	<c:set var="authorResult" value="${requestScope.authorResult}" />
  	<c:set var="comicResult" value="${requestScope.comicResult}" />
  	<c:set var="inputKeyword" value="${requestScope.inputKeyword}" />
  	
	<div class="searchbox">
    	<form id="searchkeyword" action="/searching" method="get">
        	<input type="text" class="searchinput" name="keyword" placeholder="search" />
        </form>
    </div>
    
    <div class="result">
        <p>Series</p>
        <ul class="resultul">
	        <c:choose>
	        	<c:when test="${not empty comicResult}">
	            	<c:forEach var="comic" items="${comicResult}">
						<li class="resultli">
							<a href="/comicdescription?comicId=${comic.comicId}" target="mainframe">
                                ${comic.comicName}
                            </a>
		           		</li>
					</c:forEach>
				</c:when>
				<c:otherwise>
	       			<li class="resultli">
	           			<a>No comic series found with "${inputKeyword}"</a>
	          		</li>
	          	</c:otherwise>
			</c:choose> 
        </ul>
        
        <p>Author</p>
        <ul class="resultul">
	        <c:choose>
	        	<c:when test="${not empty authorResult}">
	            	<c:forEach var="author" items="${authorResult}">
						<li class="resultli">
							<a href="/authorpage?author=${author.userId}" target="mainframe">
                                ${author.userId}
                            </a>
		           		</li>
					</c:forEach>
				</c:when>
				<c:otherwise>
	       			<li class="resultli">
	           			<a>No author found with "${inputKeyword}"</a>
	          		</li>
	          	</c:otherwise>
			</c:choose> 
        </ul>
        
    </div>
</body>
</html>
