<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 	<!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="javascripts/jquery.zlight.menu.1.0.min.js"></script> <!--reading nav js-->
	<script src="javascripts/respond.min.js"></script>
    <script src="javascripts/jquery.lazyload.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/sp.css">
    <script>
		$(document).ready(function(){
			$('#zlight-nav').zlightMenu();
		});
	</script>
	<script language="javascript">
		function aab(){
			aaa=""
			ja=""
			for(i=101,a=60;i<160;i++,a--){
				ja+="<div style=\"overflow:hidden;width:100%;height:1px;position:absolute;top:" + i + "px; left:0px; filter:alpha(opacity=" + a + "); opacity:" + (a*0.01) + "\"><img src=\"images/banner3.jpg\" style=\"margin:-" + (100-(i-100)) + "px 0px 0px 0px\"></div>"
			}
			banner.innerHTML+=ja
		}
	</script>
    <style>
		.card-title{
			font-size:14px !important;
			font-weight: 300;
		}
	</style>
</head>

<body  onload="aab()">
	<c:set var="searchingResult" value="${searchingResult}" />
	<c:set var="inputKeyword" value="${inputKeyword}" />
	
    <div id="banner">
    	<img class="bannerImg" src="images/banner3.jpg" width"100%" height="100">
    </div>
    
	<div class="ex_container">
    	<div class="ex_nav mycard">
            <div class="col-lg-12" style="padding:0">
                <nav id="zlight-nav" class="mycard">
                    <ul id="zlight-main-nav">
                        <li class="zlight-active"><a href="#">EXPLORE </a></li>
                        <li class="zlight-dropdown"><a href="#">POPULAR </a></li>
                        <li class="zlight-dropdown"><a href="#">NEW RELEASES</a></li>
                        <li class="zlight-dropdown"><a href="#">GENRES & MOODS </a></li>
                        <li class="zlight-dropdown"><a href="#">AUTHORS </a></li>
                        
                    </ul>
                    <!-- MOBILE NAV -->
                    <div id="zlight-mobile-nav">
                        <span>Menu</span>
                        <i class="icon-reorder zlight-icon"></i>
                        <select></select>
                    </div>
                </nav> <!-- nav close -->
            </div>
        </div>
        <div class="exploreContent">
            <div class="exploreSubDiv">
                <div class="ex_feature_label">
                     Searching result with ${inputKeyword}:
                </div>   
            </div>
            
            <c:choose>
				<c:when test="${not empty searchingResult}">
					<div class="row">
					<c:forEach var="result" items="${searchingResult}">
		                <div class="col s6 m4 l2">
		                	<div class="card  mycard">
		                        <div class="card-image waves-effect waves-block waves-light">
		                          <a href="/comicdescription?comicId=${result.comicId}"><img class="activator" src="images/Z-4.jpg"></a>
		                        </div>
		                        <div class="card-content">
		                          <a href="/comicdescription?comicId=${result.comicId}"><span class="card-title activator grey-text text-darken-4">${result.comicName}<i class="material-icons right">more_vert</i></span></a>
		                          <p><a href="/authorpage?author=${result.authorName}">${result.authorName}</a></p>
		                        </div>
		                        <div class="card-reveal">
		                          <span class="card-title grey-text text-darken-4">Comic Seriers Name<i class="material-icons right">close</i></span>
		                          <p>Best of the Fresh. Essential highlights from this week's Finds. surfaced by Spotify’s .</p>
		                        </div>
		          			</div>
		                </div>
					</c:forEach>
					</div>
				</c:when>
				<c:otherwise>
               		<div>
               			<span>No comic series has '${inputKeyword}' tag.</span>
               		</div>
               	</c:otherwise>
			</c:choose>
			
		</div>
	</div>
    
    <p class="copyright">© Spcomic.</p>
    <script>	
		$(function() {
			$("img.lazy").lazyload({
				effect : "fadeIn"
		 	});
		});
    </script>
   	
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
</body>
</html>