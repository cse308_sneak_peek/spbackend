<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/sp.css">
	<script src="javascripts/jquery.mixitup.min.js"></script>
    <script src=""></script>
	<title>Subscribed</title>
    <script>
		$(document).ready(function(e) {
			$(function(){
				$('#sb_comics').mixItUp();
			});
			$(".sb_item").click(function() {
				window.location = $(this).find("a").attr("href"); 
				return false;
			});
		});
	</script>
</head>

<body>
	<c:set var="subscription" value="${subscription}" />
	<c:set var="comicInfo" value="${comicInfo}" />
	
	<div>
    	<div class="container">
        	<div class="sb_feature_left">
        		<div class="sb_feature_label">Subscribed Comics  </div>  
            </div>
        	<div class="sb_feature_right">
        		<div class="sb_feature_label">Sort:  </div>  
                <div class="sb_feature_button">
                    <button class="sort button" data-sort="my-name:asc">Name</button>
                    <button class="sort button" data-sort="my-name:desc">Latest</button>
                </div>
                
            	&nbsp;&nbsp;&nbsp;
                <div class="sb_feature_label">Filter:  </div>  
                <div class="sb_feature_button">
                    <button class="filter button" data-filter=".category-1">Latest</button>
                    <button class="filter button" data-filter=".category-2">Oldest</button>
                    <button class="filter button" data-filter=".category-1, .category-2">all Category</button>
                </div>
        	</div>	
        </div>
        
        <div id="sb_comics" class="container">
			<c:choose>
				<c:when test="${not empty subscription}">
					<c:set var="index" value="0" />
					<c:forEach var="subscribe" items="${subscription}">
						<c:forEach var="comic" items="${comicInfo}" begin="${index}" end="${index}">
							<div class="sb_item mix category-1 addheight mycard" data-filter="${comic.comicName}" data-my-name="${subscribe.createdTime}">
								<div>
									<div class="sb_itembg" style="background-image:url(images/Z.png)"></div>
									<div class="sb_itemName">
										<a href="/comicdescription?comicId=${comic.comicId}">${comic.comicName}</a>
			                        </div>
			                    </div>
			                </div>
						</c:forEach>
						<c:set var="index" value="${index+1}" />
					</c:forEach>
				</c:when>
				<c:otherwise>
               		<div>
               			<span>No subscription yet.</span>
               		</div>
               	</c:otherwise>
			</c:choose>
			
        	<div class="gap"></div>
        </div>
       	
	</div>	
</body>
</html>
