<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
  <head>
    <title>Upload Chapter</title>
    <link href="../_assets/literallycanvas.css" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <style type="text/css">
      .fs-container {
        width: 720px;
        margin: auto;
      }

      .literally {
        width: 100%;
        height: 100%;
        position: relative;
      }
    </style>
  </head>

  <body>
  	<c:set var="authorResult" value="${requestScope.authorResult}" />
  	<c:set var="comicResult" value="${requestScope.comicResult}" />
  	<c:set var="inputKeyword" value="${requestScope.inputKeyword}" />
	<c:set var="comicId" value="${requestScope.comicId}" />
    <div class="uploadComic">
    	<button onclick="uploadComic1()">upload comic1</button>
    	<button onclick="uploadComic2()">upload comic2</button>
    	<button onclick="uploadComic3()">upload comic3</button>
    </div>
	
	<div class="uploadChapter">
    	<button onclick="uploadChapter1()">upload Chapter for c1</button>
    	<button onclick="uploadChapter2()">upload Chapter for c2</button>
    	<button onclick="uploadChapter3()">upload Chapter for c3</button>
    	<button onclick="uploadChapter4()">upload Chapter for c4</button>
    	<button onclick="uploadChapter5()">upload Chapter for c5</button>
    	<button onclick="uploadChapter6()">upload Chapter for c6</button>
    </div>
    
	<div class="getDescrip">
    	<button onclick="getDescription()">Go to Description</button>
    </div>
    
    <c:url value="/comicpage.jsp" var="myURL">
   		<c:param name="comicName" value="first comic series"/>
	</c:url>
	<a href="<c:url value="${myURL}"/>"></a>
	

	<!-- you really ought to include react-dom, but for react 0.14 you don't strictly have to. -->
    <script src="../_js_libs/react-0.14.3.js"></script>
    <script src="../_js_libs/literallycanvas.js"></script>
    <script src="../_js_libs/literallycanvas.js"></script>

    <script type="text/javascript">
		function getDescription() {
			document.getElementById("getComicName").value = "first comic series";
    		document.getElementById("forms_comicDescription").submit();
		}
    	function uploadComic1() {
    		document.getElementById("comicName").value = "first comic series";
    		document.getElementById("comicDescription").value = "this is my first comic series";
    		document.getElementById("comicTag1").value = "wonderful";
    		document.getElementById("comicTag2").value = "little boring";
    		document.getElementById("comicTag3").value = "cute animals";
    		document.getElementById("comicGenre").value = "Romance";
    		document.getElementById("forms_comic").submit();
    	}
    	function uploadComic2() {
    		document.getElementById("comicName").value = "horror comic series";
    		document.getElementById("comicDescription").value = "this is horror comic series";
    		document.getElementById("comicTag1").value = "ghost";
    		document.getElementById("comicTag2").value = "guyshin";
    		document.getElementById("comicTag3").value = "eggmong";
    		document.getElementById("comicGenre").value = "Horror";
    		document.getElementById("forms_comic").submit();
    	}
    	function uploadComic3() {
    		document.getElementById("comicName").value = "second comic series";
    		document.getElementById("comicDescription").value = "this is my second comic series";
    		document.getElementById("comicTag1").value = "wonderful";
    		document.getElementById("comicTag2").value = "ghost";
    		document.getElementById("comicTag3").value = "animals";
    		document.getElementById("comicGenre").value = "Humor";
    		document.getElementById("forms_comic").submit();
    	}

    	function uploadChapter1() {    		
    		document.getElementById("chapterName").value = "first chapter";
    		document.getElementById("parentName").value = "first comic series";
    		document.getElementById("forms_chapter").submit();
    	}
    	function uploadChapter2() {
    		document.getElementById("chapterName").value = "second chapter";
    		document.getElementById("parentName").value = "first comic series";
    		document.getElementById("forms_chapter").submit();
    	}
    	function uploadChapter3() {    		
    		document.getElementById("chapterName").value = "start story";
    		document.getElementById("parentName").value = "horror comic series";
    		document.getElementById("forms_chapter").submit();
    	}
    	function uploadChapter4() {
    		document.getElementById("chapterName").value = "end story";
    		document.getElementById("parentName").value = "horror comic series";
    		document.getElementById("forms_chapter").submit();
    	}
    	function uploadChapter5() {    		
    		document.getElementById("chapterName").value = "1 story";
    		document.getElementById("parentName").value = "second comic series";
    		document.getElementById("forms_chapter").submit();
    	}
    	function uploadChapter6() {
    		document.getElementById("chapterName").value = "2 story";
    		document.getElementById("parentName").value = "second comic series";
    		document.getElementById("forms_chapter").submit();
    	}
    	
    </script>
    <form id="forms_comic" action="/comic" method="post">
        <input type="hidden" id="authorId" name="authorId">
        <input type="hidden" id="comicId" name="comicId">
        <input type="hidden" id="comicName" name="comicName">
        <input type="hidden" id="comicDescription" name="comicDescription">
        <input type="hidden" id="comicTag1" name="comicTag1">
        <input type="hidden" id="comicTag2" name="comicTag2">
        <input type="hidden" id="comicTag3" name="comicTag3">
        <input type="hidden" id="comicGenre" name="comicGenre">
    </form>

	<form id="forms_chapter" action="/chapter" method="post">
        <input type="hidden" id="chapterNum" name="chapterNum">
        <input type="hidden" id="chapterName" name="chapterName">
        <input type="hidden" id="parentName" name="comicName">
        <input type="hidden" name="comicId" value="${comicId }">
    </form>
    
    <form id="forms_comicDescription" action="/comicdescription" method="get">
    	<input type="hidden" id="getComicName" name="comicName">
    </form>
    
	<c:if test="${not empty chapterList}">
		<c:forEach var="chapter" items="${chapterList}">
		
			${chapter.chapterNum} ${chapter.chapterName} ${chapter.createdTime} <br>
		
		</c:forEach>
	</c:if>
	
	<form id="forms_searching" action="/searching" method="get">
    	Searching Keyword: <input type="text" name="keyword" />
    </form>
    
    <div>
  		<table>
  			<tbody>
  				<c:set var="inputKeyword" value="${inputKeyword}" />
  				<p>Result with '${inputKeyword}':</p>
               	<c:choose>
               	 	<c:when test="${not empty authorResult}">
	               		<p>[AUTHORS]</p>
	               		<tr>
		                	<th align="center">User ID</th>
	                	</tr>
	               		<c:forEach var="author" items="${authorResult}">
							<tr>
	                   			<td align="center">${author.userId}</td>
	                		</tr>
						</c:forEach>
	               	</c:when>
	               	<c:otherwise>
	              			No authors found with '${inputKeyword}'
	               	</c:otherwise>
               </c:choose>
               <c:choose>
               	 	<c:when test="${not empty comicResult}">
               	 		<p>[COMIC SERIES]</p>
	               		<tr>
		                	<th align="center">Comic Name</th>
		                    <th align="center">Comic Genre</th>
		                    <th align="center">Tags</th>	
	                	</tr>
	               		<c:forEach var="comic" items="${comicResult}">
							<tr>
	                   			<td align="center">${comic.comicName}</td>
	                   			<td align="center">${comic.comicGenre}</td>
	                       		<td align="center">${comic.tags}</td>
	                		</tr>
						</c:forEach>
	               	</c:when>
	               	<c:otherwise>
	              			No comic series found with '${inputKeyword}'
	               	</c:otherwise>
               </c:choose>
              </tbody>
          </table>
    </div>
    
  </body>
</html>