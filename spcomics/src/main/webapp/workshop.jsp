<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/sp.css">    
	<title>Workshop</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/sp.css">
    <title>Comic Setting</title>
    <link href="css/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="javascripts/tag-it.js" type="text/javascript" charset="utf-8"></script>
    <script>
    	$(document).ready(function() {
    		$("ul li a").click(function(e) {
				e.preventDefault();
                if($(this).attr("id") == "createNewSeries"){
                    $("#workshopFrame").load("comic_setting.jsp");
                }
                else{
                    $("#workshopFrame").attr("src", $(this).attr("href"));
                }
			});

            $(".series_item").click(function(event){
                var str=$(this).children("div:last");
                str=str.html();
                str=str.trim();
                str=str.split(' ').join('_');
                //alert(str);
                if(str !="New_Series"){
                    var url="/loadworkshopajax?input="+str;
                    $("#workshopFrame").load(url);
                }
            });
		});
	</script>
</head>

<body>
	<div class="ws_left">
    	<div class="series_list mycard">
        	<ul>
            	<li>
                	<a id="createNewSeries">
                        <div class="series_item">
                            <div class="item_cover" style="background-image:url(images/plus_sign.ico); background-size:50% 50%;">
                            </div>
                            
                            <div class="item_text">
                            	New Series
                            </div>
                        </div>
                    </a>
                </li>
                <c:if test="${not empty lst}">
                    <c:forEach var="items" items="${lst}">
                        <li>
                            <a href="#">
                                <div class="series_item">
                                    <div class="item_cover" style="background-image:url(images/plus_sign.ico); background-size:50% 50%;">
                                    </div>

                                    <div class="item_text">
                                        ${items.comicName}
                                    </div>
                                </div>
                            </a>
                        </li>
                    </c:forEach>
                </c:if>

            </ul>
        </div>
    </div>
    
    <div class="ws_right">
        <div id="workshopFrame" class="workshopFrame" name="workshopFrame" src="Workshop_Comic_Series.jsp"></div>
    </div>
    
</body>
</html>