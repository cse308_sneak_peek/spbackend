<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<!DOCTYPE html html>
<html>
<head>
    <title>Literally Canvas</title>

    <!--Materialize-->
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript" src="javascripts/jquery-2.2.2.js"></script>
    <link href="../_assets/literallycanvas.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="css/sp.css">
    <meta charset="UTF-8">
    <style>
		.literally {
			height: 100%;
			position: relative;
		}
	</style>
</head>

<body> 
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<div class="row">
		<div class="showItems col l2 l2" style="margin-top:10px">
			<table id="w_table" class="highlight mycard">
				<thead>
					<tr>
						<th> Item Name </th>
						<th> Display </th>
					</tr>
				</thead>
				<tbody>
					<c:if test="${not empty workshoplist}">
						<c:forEach var="item" items="${workshoplist}">
							<c:if test="${item.userId eq sessionScope.user_info.userId}">
								<div id="workshop_table">
									<tr>
										<td width="70%" valign="top" align="left"><button class="btn btn-primary waves-effect waves-light" id="username"
												onclick='getObjectImage(${item.value}, ${item.name})'>${item.name}</button></td>
										<td width="30%"><img height="50" width="50" src="${item.secondaryValue}" /></td>
									</tr>
								</div>
							</c:if>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
		</div>
		
		<div class="fs-container col l8 l8">
			<div class="mycard" style="color:black; margin:10px 0 10px 0; padding:10px;">
				<span>
					Use template: 
				</span>
				<button class="btn btn-primary waves-effect waves-light  pink accent-1" type="button" onclick="loadTemplate1()">Template #1</button>
				<button class="btn btn-primary waves-effect waves-light  pink accent-1" type="button" onclick="loadTemplate2()">Template #2</button>
				<button class="btn btn-primary waves-effect waves-light  pink accent-1" type="button" onclick="loadTemplate3()">Template #3</button>
			</div>
			<div id="lc" class="mycard" style="height:90%;">
				<div id="svg-wrapper"></div>
			</div>
			
			<div class="controls export" style="margin-top:10px">
				<button class="btn btn-primary waves-effect waves-light" onclick="exportSVG()">Save Template</button>
				<button class="btn btn-primary waves-effect waves-light" onclick="saveChapter()">Save Chapter Page</button>
				<div class="file-field input-field">
					<div class="btn">
						<span>Choose File</span>
						<input id="inputImage" class="btn" type="file" accept="image/png" value="Load Local Image">
					</div>
					
					<div class="file-path-wrapper" style="float:left">
						<input class="file-path validate" type="text">
					</div>
					<div class="btn btn-primary waves-effect waves-light teal lighten-1" style="float:left; margin-left:10px">
						<button class="teal lighten-1" type="button" onclick="loadImage()" >Use Image</button>
					</div>
				</div>
				
				<!-- <input id="inputImage" class="btn btn-primary waves-effect waves-light" type="file" accept="image/png" value="Load Local Image" onchange="loadImage()"> -->
			</div>
			
			<div id="imgTest" style="visibility:hidden"></div>
			<div id="imgShow"></div>
		</div>
		
		<div class="right_toolbar col l2 l2">
			<ul id="sortable">
				<c:if test="${not empty currentChapterPages}">
					<c:forEach items="${currentChapterPages}" var="item" varStatus="status">
						<li> 
							<img height="50" width="300" src="${item}" /> 
						</li>
					</c:forEach>
				</c:if>
			</ul>
			<div class="series_list mycard" width="100%">
				<ul>
					<li>
						<a id="addNewPage">
							<div class="series_item">
								<div class="item_cover" style="background-image: url(images/plus_sign.ico); background-size: 50% 50%;"> </div>
								<div class="item_text">Add Page</div>
							</div>
						</a>
					</li>
				</ul>
			</div>
			
			<ol>
				<c:if test="${not empty currentChapterPagesLoad}">
					<c:forEach items="${currentChapterPagesLoad}" var="items" varStatus="status">
						<li onclick='loadIntoCanvas(${items}, ${status.index})'>
							<p class="btn btn-primary waves-effect waves-light">Load Page</p>
						</li>
					</c:forEach>
				</c:if>
			</ol>
		</div>
	</div>
	
	
	
 
	
<!-- you really ought to include react-dom, but for react 0.14 you don't strictly have to. -->
	<script src="../_js_libs/react-0.14.3.js"></script>
	<script src="../_js_libs/literallycanvas.js"></script>
	<script src="../_js_libs/literallycanvas.js"></script>
	<script type="text/javascript">
	/*
	<input type="file" onchange="previewFile()"><br>
		<img src="" height="200" alt="Image preview...">
		
		<input id="inputFileToLoad" type="file" onchange="encodeImageFileAsURL();" />
		<div id="imgTest"></div>
		
		<input type="file" id="files" name="files[]" multiple />
		<output id="list"></output>
	*/

    	var lc = LC.init(document.getElementById("lc"), {
        	imageURLPrefix: '../_assets/lc-images',
      		toolbarPosition: 'bottom',
      		secondaryColor:'transparent',
       		defaultStrokeWidth: 2,
       		strokeWidths: [1, 2, 3, 5, 30]
    	});
	
    	var localStorageKey = 'drawing'
    	/*
		<c:if test="${not empty requestScope.a}">
        <c:set var="xvalue" value="${requestScope.a.value}"></c:set>
        </c:if>
		*/
    	if (localStorage.getItem(localStorageKey)) {
    	  lc.loadSnapshot(JSON.parse(localStorage.getItem(localStorageKey)));
    	}
    	lc.on('drawingChange', function() {
    		localStorage.setItem(localStorageKey, JSON.stringify(lc.getSnapshot()));
    	});
    	/*
    	function handleFileSelect(evt) {
    	    var files = evt.target.files; // FileList object
    	    // files is a FileList of File objects. List some properties.
    	    var output = [];
    	    for (var i = 0, f; f = files[i]; i++) {
    	    	var reader = new FileReader();
    	    	reader.readAsDataURL(f);
    	      	output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
    	                  f.size, ' bytes, last modified: ',
    	                  f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
    	                  '</li>');
    	    }
    	    document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
    	}
    	*/
		//document.getElementById('files').addEventListener('change', handleFileSelect, false);
    	function loadTemplate1(){
    		lc.clear();
    		var imgSrc = "/images/template1.jpg";
    		var newImage = new Image();
    		newImage.src=imgSrc;
	        lc.saveShape(LC.createShape('Image',{x:10,y:10,image:newImage}));
	    }
		function loadTemplate2(){
    		lc.clear();
    		var imgSrc = "/images/template2.jpg";
    		var newImage = new Image();
    		newImage.src=imgSrc;
	        lc.saveShape(LC.createShape('Image',{x:10,y:10,image:newImage}));
    	}
		function loadTemplate3(){
    		lc.clear();
    		var imgSrc = "/images/template3.jpg";
    		var newImage = new Image();
    		newImage.src=imgSrc;
	        lc.saveShape(LC.createShape('Image',{x:10,y:10,image:newImage}));
    	}
    	function saveChapter(){
    		var currentIndex = '${sessionScope.currentIndex}';
    		var newPage = lc.getImage().toDataURL();
    		var newPageLoad = JSON.stringify(lc.getSnapshot());
    		if(currentIndex==""){
    			alert("Please add page before attempting to save");
    		}
    		else{
    			document.getElementById("updateIndex").value=currentIndex;
    			document.getElementById("updatePage").value=newPage;
    			document.getElementById("updatePageLoad").value=newPageLoad;
    			document.getElementById("pageImages").submit();
    		}
    	}
    	function loadIntoCanvas(input,number){
    		//console.log("HELLO " + number);
    		document.getElementById("currentIndex").value=number;
    		document.getElementById("pageUpdate").submit();
    		lc.loadSnapshot(input);
    		//document.getElementById("pageNumber").value=number;
    	}
    	function exportPNG(){
    		window.open(lc.getImage().toDataURL());
            //alert("hi");
            document.getElementById("userId").value="joan";
            document.getElementById("content").value=lc.getImage().toDataURL();
            document.getElementById("type").value="dataURL";
            console.log("This is bullshit");
            console.log("This is bullshit Again");
            //window.open("data:image/svg+xml;base64," + btoa(svgString));
            //document.getElementById("forms").submit();
    	}
    	/*
    	function encodeImageFileAsURL(){

    	    var filesSelected = document.getElementById("inputFileToLoad").files;
    	    if (filesSelected.length > 0)
    	    {
    	        var fileToLoad = filesSelected[0];

    	        var fileReader = new FileReader();

    	        fileReader.onload = function(fileLoadedEvent) {
    	            var srcData = fileLoadedEvent.target.result; // <--- data: base64

    	            var newImage = document.createElement('img');
    	            newImage.src = srcData;
    	            //lc.saveShape(LC.createShape('Image',{x:0,y:0,image:newImage}));
    	        	var dataURL=document.getElementById("imgTest").innerHTML;
    	            document.getElementById("imgTest").innerHTML = newImage.outerHTML;
    	            alert("Converted Base64 version is "+JSON.stringify(document.getElementById("imgTest").innerHTML).substring(34));
    	            console.log("Converted Base64 version is "+document.getElementById("imgTest").innerHTML);
    	        	//lc.loadSnapshot(JSON.parse(JSON.stringify((document.getElementById("imgTest").innerHTML).substring(indexOf(",")))));	
    	        }
    	        fileReader.readAsDataURL(fileToLoad);
    	    }
    	}
    	*/
    	function loadImage(){
    	    /*
    		var newImage = new Image();
    	    newImage.src = 'https://mdn.mozillademos.org/files/5397/rhino.jpg';
    	    lc.saveShape(LC.createShape('Image', {x: 10, y: 10, image: newImage}));
    		*/
    	    //var oCanvasdd = document.getElementById("lc")[0];
    		var filesSelected = document.getElementById("inputImage").files;
    		inputFile = filesSelected[0];
    		console.log(inputFile);
    		var fileReader = new FileReader();
	        fileReader.onload = function(fileLoadedEvent) {
	            var srcData = fileLoadedEvent.target.result; // <--- data: base64
	            var newImage = document.createElement('img');
	            newImage.style.visibility="hidden";
	            newImage.src = srcData;
	            //lc.saveShape(LC.createShape('Image',{x:0,y:0,image:newImage}));
	        	var dataURL=document.getElementById("imgTest").innerHTML;
	            document.getElementById("imgTest").innerHTML = newImage.outerHTML;
	            alert("Converted Base64 version is "+JSON.stringify(document.getElementById("imgTest").innerHTML).substring(34));
	            console.log("Converted Base64 version is "+document.getElementById("imgTest").innerHTML);
	        	lc.saveShape(LC.createShape('Image',{x:10,y:10,image:newImage}));
	            //lc.loadSnapshot(JSON.parse(JSON.stringify((document.getElementById("imgTest").innerHTML).substring(indexOf(",")))));	
	        }
	        fileReader.readAsDataURL(inputFile);
    		//var inputString = fileReader.readAsDataURL(inputFile);
    		//var base64Image = "data:image/svg+xml;base64,"+inputString;
    		//alert(inputString);
    		/*
    		if (filesSelected.length > 0){
    			console.log("Hi I'm here");
    	        var fileToLoad = filesSelected[0];
    	        console.log("This is "+fileToLoad);
    	        var fileReader = new FileReader();
    	        fileReader.readAsDataURL
        	    //newImage.src = srcData;
    	    	fileReader.onload=function(fileLoadedEvent){
        	    	var srcData = fileLoadedEvent.target.result;
        	    	console.log("SrcData is "+srcData);
        	    	newImage.src = srcData;
        	    }
        	}
    		*/
    	    //newImage.src = 'https://mdn.mozillademos.org/files/5397/rhino.jpg';
    	}
    	/*
    	function previewFile() {
    		  var preview = document.querySelector('img');
    		  var file    = document.querySelector('input[type=file]').files[0];
    		  var reader  = new FileReader();

    		  reader.addEventListener("load", function () {
    		    preview.src = reader.result;
    		  }, false);

    		  if (file) {
    		    //lc.loadSnapshot(JSON.parse(JSON.stringify(reader.readAsDataURL(file))));
    		    reader.readAsDataURL(file);
    		  }
    		}
    	*/
    	function getObjectImage(value,name){
    		//var sessionName = JSON.stringify('${sessionScope.user_info.userId}');
    		//if(name===sessionName){
    			lc.loadSnapshot(JSON.parse(JSON.stringify(value)));
    			//console.log(JSON.stringify(value));
    		//}
    	}
    	function scaleDown(){
    		var svgString = JSON.stringify(lc.getSnapshot());
    		//var abs='"rotation":90,';
    		var scaleDown = '"scale":0.5'
    		var newString = svgString.replace('"scale":1',scaleDown);
    		if(svgString.indexOf('"scale":1')> -1){
    			newString = svgString.replace('"scale":1',scaleDown);
    		}
    		else if(svgString.indexOf('"scale":2')> -1){
    			newString = svgString.replace('"scale":2',scaleDown);
    		}
    		//alert(newString);
    		//console.log(output);
    		lc.loadSnapshot(JSON.parse(newString));
    		//console.log(abs);
    	}
    	function scaleToNormal(){
    		var svgString = JSON.stringify(lc.getSnapshot());
    		//var abs='"rotation":90,';
    		var scaleUp = '"scale":1'
    		if(svgString.indexOf('"scale":2')> -1){
    			newString = svgString.replace('"scale":2',scaleUp);
    		}
    		else if(svgString.indexOf('"scale":0.5')> -1){
    			newString = svgString.replace('"scale":0.5',scaleUp);
    		}
    		//alert(newString);
    		console.log("Hello");
    		lc.loadSnapshot(JSON.parse(newString));
    	
    	}
    	function scaleUp(){
    		var svgString = JSON.stringify(lc.getSnapshot());
    		//var abs='"rotation":90,';
    		var scaleUp = '"scale":2'
    		if(svgString.indexOf('"scale":1')> -1){
    			newString = svgString.replace('"scale":1',scaleUp);
    		}
    		else if(svgString.indexOf('"scale":0.5')> -1){
    			newString = svgString.replace('"scale":0.5',scaleUp);
    		}
    		//alert(newString);
    		//console.log(output);
    		lc.loadSnapshot(JSON.parse(newString));
    		//console.log(abs);
    	}
    	function rotate90(){
    		var svgString = JSON.stringify(lc.getSnapshot());
    		var abs='"angle":20,';
    		//var scaleDown = '"scale":0.5'
    		var newString = [svgString.slice(0,1),abs,svgString.slice(1)].join('');
    		alert(newString);
    		//console.log(output);
    		lc.loadSnapshot(JSON.parse(newString));
    		//console.log(abs);
    	}
    	function exportSVG(){
            var svgString = lc.getSVGString();
            //document.getElementById("userId").value= user.getEmail().split("@")[0];
            var objectName = prompt("Enter a name for the object");
            if(objectName!=null){
            	if (objectName===""||objectName===null){
            		alert("Please enter a name for your object!");
            	}
            	else{
            		var name = JSON.stringify(objectName);
            		var userId = "${sessionScope.user_info.userId}";
            		console.log(JSON.stringify(userId));
            		document.getElementById("userId").value=userId;
            		document.getElementById("name").value=name;
            		document.getElementById("secondaryValue").value=lc.getImage().toDataURL();
            		document.getElementById("content").value=JSON.stringify(lc.getSnapshot());
                	document.getElementById("type").value="jsonString";
                	document.getElementById("forms").submit();
            	}
            }
            else{
            	return;
            }
            	//window.open("data:image/svg+xml;base64," + btoa(svgString));
        }
    	
    	$(document).ready(function() {
    		$("ul li a").click(function(e) {
				e.preventDefault();
				/*
				Add default empty page when the button is clicked
				*/
                if($(this).attr("id") == "addNewPage"){
                	console.log(lc.getImage().toDataURL());
                    document.getElementById("newPage").value=lc.getImage().toDataURL(); //This is the base64 String that can be easily downloaded as a PNG
                    document.getElementById("newPageLoad").value=JSON.stringify(lc.getSnapshot()); //This is the JSON that the user loads into the canvas
                	document.getElementById("newPageForm").submit();
                }
                else{
                    //$("#workshopFrame").attr("src", $(this).attr("href"));
                }

			});
    	});
    	/*
        function a(){
            var temp="";

            <c:choose>
            <c:when test="empty xvalue">
            temp="";
            </c:when>
            <c:otherwise>
            temp='${xvalue}';
            </c:otherwise>

            </c:choose>


            if(temp==""){
                alert("nothing")
            }else{
                alert(JSON.stringify(${xvalue}));

                if(temp!=null){
                    lc.loadSnapshot(JSON.parse(JSON.stringify(${xvalue})));
                }
            }
        }
    	*/
    </script>
	<form id="forms" action="/workshop" method="post">
		<input type="hidden" id="userId" name="userId"> 
		<input type="hidden" id="content" name="content">
		<input type="hidden" id="secondaryValue" name="secondaryValue">
		<input type="hidden" id="type" name="type">
		<input type="hidden" id="name" name="name">
	</form>
	<form id="newPageForm" action="/chapterPage" method="post">
		<input type="hidden" id="newPage" name="newPage"> 
		<input type="hidden" id="newPageLoad" name="newPageLoad">
	</form>
	<form id="pageUpdate" action="/pageUpdate" method="post">
		<input type="hidden" id="currentIndex" name="currentIndex">
	</form>
	<form id="pageImages" action="/pageImages" method="post">
		<input type="hidden" id="updateIndex" name="updateIndex">
		<input type="hidden" id="updatePage" name="updatePage">
		<input type="hidden" id="updatePageLoad" name="updatePageLoad">
	</form>
</body>
</html>